﻿using System.Collections;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.VFX;

namespace Spiritway.Spirit
{
    public class SpiritFireIntensity : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] float intensity;
        [SerializeField] float defaultTransitionDuration;

        [Header("Changing components")] 
        [SerializeField] VisualEffect fire;
        [SerializeField] VisualEffect fireRoot;
        [SerializeField] MeshRenderer firePlane;
        [SerializeField] MeshRenderer trail;
        
        void Update()
        {
            // use intensity to update fire shader and VFX
            fire.SetFloat("Visibility", intensity);
            fireRoot.SetFloat("Visibility", intensity);
            firePlane.material.SetFloat("Visibility", intensity);
            trail.material.SetFloat("Opacity", intensity);
        }

        public IEnumerator Appear()
        {
            for (float elapsed = 0; elapsed < defaultTransitionDuration; elapsed += Time.deltaTime)
            {
                intensity = elapsed / defaultTransitionDuration;
                yield return null;
            }

            intensity = 1;
        }

        public IEnumerator Disappear()
        {
            for (float elapsed = 0; elapsed < defaultTransitionDuration; elapsed += Time.deltaTime)
            {
                intensity = 1 - elapsed / defaultTransitionDuration;
                yield return null;
            }

            intensity = 0;
        }
    }
}