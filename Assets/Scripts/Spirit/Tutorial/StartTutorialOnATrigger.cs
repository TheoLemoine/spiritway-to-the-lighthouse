﻿using System.Linq;
using Spiritway.Boat;
using Spiritway.UI;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.Spirit.Tutorial
{
    public class StartTutorialOnATrigger : ATriggerOnce
    {
        [SerializeField] GlobalEvent tutorialEvent;
        [SerializeField, TagSelector] string boatTag;
        [SerializeField] VisualTreeAsset uiScreen;
        [SerializeField] UIManager uiManager;

        BoatEngine _engine;
        VisualElement _hudScreen;
        
        protected override void Start()
        {
            base.Start();

            _hudScreen = uiScreen.Instantiate().Children().First();
            _engine = GameObject.FindWithTag(boatTag).GetComponentInChildren<BoatEngine>();
        }

        protected override void OnTrigger()
        {
            Debug.Log($"Hello {tutorialEvent.WasTriggered}");
            
            if(tutorialEvent.WasTriggered) return;

            _engine.Gear = 0;
            
            var tip = _hudScreen.Q("tutorial_projector_tip");
            uiManager.AddScreenToHud(_hudScreen);
            StartCoroutine(UIHelper.SmoothLerpOpacity(tip, 0, 1, 1));
            StartCoroutine(TimeHelper.Delay(8f, UIHelper.SmoothLerpOpacity(tip, 1, 0, 1)));

            StartCoroutine(TimeHelper.Delay(2f, () => tutorialEvent.Call()));
        }
    }
}