﻿using System;
using Spiritway.Dialogue;
using Spiritway.Sound;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Spirit.Tutorial
{
    public class SpiritTutorial : MonoBehaviour
    {
        [SerializeField] SpiritFireIntensity fireIntensity;
        [SerializeField] GlobalEvent startTutorialEvent;
        [SerializeField] SpiritDetectable detectable;

        [Header("Sound")] 
        [SerializeField] SoundManager soundManager;
        [SerializeField] DialogueLine sailorTutorialLine;
        [SerializeField] GameObject spiritSoundObject;
        [SerializeField] AK.Wwise.Switch spiritNone;
        [SerializeField] AK.Wwise.Switch spiritWillOWisp;

        bool _wasStarted;

        void Awake()
        {
            spiritNone.SetValue(spiritSoundObject);
        }

        void Start()
        {
            startTutorialEvent.OnCalled += StartSpiritTutorial;
            detectable.OnDetected += EndSpiritTutorial;
        }

        void OnDestroy()
        {
            startTutorialEvent.OnCalled -= StartSpiritTutorial;
            detectable.OnDetected -= EndSpiritTutorial;
        }

        void StartSpiritTutorial()
        {
            if(_wasStarted) return;
            _wasStarted = true;

            StartCoroutine(TimeHelper.Delay(1, soundManager.PlayLine(sailorTutorialLine, gameObject)));
            spiritWillOWisp.SetValue(spiritSoundObject);
            transform.parent = transform.parent.parent;
            StartCoroutine(fireIntensity.Appear());
        }
        
        void EndSpiritTutorial()
        {
            spiritNone.SetValue(spiritSoundObject);
            StartCoroutine(fireIntensity.Disappear());
        }
    }
}