﻿using System;
using Spiritway.Utils.Helpers;
using UnityEngine;
using Event = AK.Wwise.Event;

namespace Spiritway.Spirit
{
    public class SpiritPath : MonoBehaviour
    {
        [SerializeField] Transform[] targets;
        [SerializeField] SpiritController controller;

        public event Action OnLastDestination;
        public event Action OnPathFinished;
        
        public int NextTargetIndex { get; set; }
        public bool Finished { get; private set; }

        void Start()
        {
            controller.OnDestinationReached += NextDestination;
        }

        public void RunThrough()
        {
            NextDestination();
        }

        void NextDestination()
        {
            if (NextTargetIndex == targets.Length - 1)
            {
                OnLastDestination?.Invoke();
            }
            
            if (NextTargetIndex >= targets.Length)
            {
                controller.Stop();
                OnPathFinished?.Invoke();
                Finished = true;
                return;
            }
            
            controller.SetTarget(targets[NextTargetIndex].position);
            NextTargetIndex++;
        }

#if UNITY_EDITOR
        
        void OnDrawGizmos()
        {
            if(controller == null) return;
            
            Gizmos.color = Color.cyan;

            if (targets.Length > 0)
            {
                Gizmos.DrawLine(transform.position, targets[0].position);
            }
            
            for (int i = 0; i < targets.Length; i++)
            {
                if(i < targets.Length - 1)
                    Gizmos.DrawLine(targets[i].position, targets[i+1].position);
                
                Gizmos.DrawSphere(targets[i].position, controller.stopDistance);
                Gizmos.DrawWireSphere(targets[i].position, controller.brakeDistance);
            }
        }
        
#endif
    }
}