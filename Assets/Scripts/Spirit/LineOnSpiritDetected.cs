﻿using System;
using Spiritway.Dialogue;
using Spiritway.Sound;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Spirit
{
    public class LineOnSpiritDetected : MonoBehaviour
    {
        [SerializeField] SpiritDetectable detectable;
        [SerializeField] DialogueLine line;
        [SerializeField] SoundManager manager;

        void Start()
        {
            detectable.OnDetected += PlayLine;
        }

        void PlayLine()
        {
            StartCoroutine(TimeHelper.Delay(2f, manager.PlayLine(line, gameObject)));
        }
    }
}