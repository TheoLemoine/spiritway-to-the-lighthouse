﻿using Spiritway.Respawn;
using UnityEngine;

namespace Spiritway.Spirit
{
    public class SpiritRespawn : ARespawnableBehaviour
    {
        [SerializeField] SpiritPath path;
        [SerializeField] SpiritController controller;
        [SerializeField] SpiritDetectable detectable;
        [SerializeField] Transform spiritTransform;
        
        Vector3 _position;
        Quaternion _rotation;
        
        int _pathNextTargetIndex;
        bool _activated;
        bool _finished;

        public override void Save()
        {
            _finished = path.Finished;
            _pathNextTargetIndex = path.NextTargetIndex;
            
            _position = spiritTransform.position;
            _rotation = spiritTransform.rotation;
        }

        public override void Respawn()
        {
            if (_finished)
            {
                gameObject.SetActive(false);
                return;
            }
            
            spiritTransform.position = _position;
            spiritTransform.rotation = _rotation;
            
            detectable.TransformToFirefly();
            controller.Stop();
            path.NextTargetIndex = Mathf.Max(0, _pathNextTargetIndex - 1);
        }
    }
}