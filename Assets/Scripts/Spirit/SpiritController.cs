﻿using System;
using UnityEngine;

namespace Spiritway.Spirit
{
    [RequireComponent(typeof(Rigidbody))]
    public class SpiritController : MonoBehaviour
    {
        [SerializeField] float maxForwardForce = 3.5f;
        [SerializeField] float maxTorquePerSpeed = 0.2f;
        [SerializeField] public float brakeDistance = 3f;
        [SerializeField] public float stopDistance = 1.5f;

        public Vector3 Target { get; private set; }
        public bool Active { get; private set; }

        Rigidbody _rb;
        Transform _transform;

        public event Action OnDestinationReached;

        public void SetTarget(Vector3 target)
        {
            Active = true;
            Target = target;
        }

        public void Stop()
        {
            Active = false;
        }
        
        void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _transform = GetComponent<Transform>();
        }

        void FixedUpdate()
        {
            if(!Active) return;
            
            var toTarget = Target - _transform.position;
            var spiritForward = _transform.forward;

            if (toTarget.sqrMagnitude < stopDistance * stopDistance)
            {
                OnDestinationReached?.Invoke();
                return;
            }
            
            // compute engine force
            var distToTarget = toTarget.magnitude;
            var forwardForce = maxForwardForce * Mathf.Min(distToTarget, brakeDistance) / brakeDistance;
            
            // compute angle
            var toTargetUpView = new Vector2(toTarget.x, toTarget.z);
            var forwardUpView = new Vector2(spiritForward.x, spiritForward.z);

            var angleToTarget = Vector2.SignedAngle(forwardUpView, toTargetUpView);

            var torquePerSpeed = Mathf.Clamp(angleToTarget, -maxTorquePerSpeed, maxTorquePerSpeed);

            // 
            _rb.AddRelativeForce(0, 0, forwardForce, ForceMode.Acceleration);

            var forwardVel = _transform.InverseTransformDirection(_rb.velocity).z;
            _rb.AddRelativeTorque(0, forwardVel * -torquePerSpeed, 0, ForceMode.Acceleration);
        }
    }
}