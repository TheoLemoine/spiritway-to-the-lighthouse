﻿using System;
using Spiritway.Boat.Projector;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Spirit
{
    public class SpiritDetectable : AProjectorDetectable
    {
        [SerializeField] float startPathDelay = 3f;
        [SerializeField] SpiritPath path;
        [SerializeField] Animator ghostAnimator;
        [SerializeField] Rigidbody spiritRigidbody;
        [SerializeField] bool activateRigidbody = true;

        [Header("Sound")]
        [SerializeField] AK.Wwise.Switch willOWispSwitch;
        [SerializeField] AK.Wwise.Switch ghostSwitch;
        [SerializeField] AK.Wwise.Switch endSwitch;
        
        static readonly int Appeared = Animator.StringToHash("Appeared");
        static readonly int Finished = Animator.StringToHash("Finished");

        public event Action OnDetected;

        public bool Detected { get; set; }

        protected override void Start()
        {
            base.Start();

            spiritRigidbody.isKinematic = true;
        }

        protected override void OnProjectorVisibilityGained()
        {
            if (Detected) return;
            
            OnDetected?.Invoke();
            TransformToBoat();
            
            StartCoroutine(TimeHelper.Delay(startPathDelay, () =>
            {
                path.RunThrough();
                path.OnLastDestination += TurnOff;
            }));
        }

        public void TransformToBoat()
        {
            Detected = true;
            ghostAnimator.SetBool(Appeared, true);
            ghostSwitch.SetValue(spiritRigidbody.gameObject);
            
            if (activateRigidbody)
            {
                spiritRigidbody.isKinematic = false;
            }
        }

        public void TransformToFirefly()
        {
            Detected = false;
            ghostAnimator.SetBool(Appeared, false);
            willOWispSwitch.SetValue(spiritRigidbody.gameObject);
            
            if (activateRigidbody)
            {
                spiritRigidbody.isKinematic = true;
            }
        }

        public void TurnOff()
        {
            ghostAnimator.SetBool(Finished, true);
            endSwitch.SetValue(spiritRigidbody.gameObject);
            //spiritRigidbody.isKinematic = true;
        }
    }
}