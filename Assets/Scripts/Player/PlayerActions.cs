// GENERATED AUTOMATICALLY FROM 'Assets/Settings/PlayerActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Spiritway.Player
{
    public class @PlayerActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @PlayerActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerActions"",
    ""maps"": [
        {
            ""name"": ""SailorControls"",
            ""id"": ""137c6644-0671-42b3-b45a-0bf1a67c6166"",
            ""actions"": [
                {
                    ""name"": ""Walk"",
                    ""type"": ""Value"",
                    ""id"": ""e8727eba-1335-4f6d-9e06-dd5efa5ecf46"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""73a4557c-b96c-488e-8394-69f8246122ca"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Value"",
                    ""id"": ""41d9f43e-2f4f-46ad-a700-878f404be478"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AutoLook"",
                    ""type"": ""Button"",
                    ""id"": ""2ed9d270-6099-4c97-9308-ea062e0d7be9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""903b76d4-f356-4f47-8266-b7917e441969"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""818a1c6c-3d50-4fb1-8760-c8bef95846b3"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""right"",
                    ""id"": ""388888db-f616-487c-aa63-e6298aa46c8d"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""up"",
                    ""id"": ""f87bf456-d0fa-4228-b29b-17928d70577b"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""ab100d82-9ea0-48e6-be73-3fee4e797da7"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""7c9794c4-7cf8-4ade-9378-d2cc13d60d06"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""d1b3a3b8-3e1c-48ba-92fe-081759c83459"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a030c70-c07a-4711-bb14-4357708ec5f0"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a2ac3b4-00e6-43d3-9553-ca060de9ed69"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=10,y=8)"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""02446f96-1c12-481c-ab30-bf749df0ae10"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fe4d1e6d-64fb-4403-88d8-238f8dcd2239"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c545cc80-2b68-45cd-b895-5bea15489dbe"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""15688896-7da4-497e-b00f-bfa77360e489"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10940642-a429-4aac-a9ee-04ac32d52985"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""AutoLook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""869157d5-6c79-4e52-a7be-0db87933af6f"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""AutoLook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e04cb5c4-fa77-4daa-9377-d4d153839573"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e09e497-7d47-4b39-8ac0-7dcaa6d6d916"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""BoatControls"",
            ""id"": ""300265ca-1a59-45cf-96d8-646b0811e216"",
            ""actions"": [
                {
                    ""name"": ""Rudder"",
                    ""type"": ""Value"",
                    ""id"": ""51f5d5c3-e59e-4a74-a9cb-cb9895d0dcc1"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Gearshift_Up"",
                    ""type"": ""Button"",
                    ""id"": ""8d03b3db-2b8f-45dd-9cbe-ca187988c69f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Gearshift_Down"",
                    ""type"": ""Button"",
                    ""id"": ""a0f300c0-5af8-4f17-9744-f3916d11672c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""458717f3-a21a-4e85-a1da-fe3cc148e5e9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Siren"",
                    ""type"": ""Button"",
                    ""id"": ""5ea4f96d-4a3e-443e-b9bb-be75ac47ffc5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""AD"",
                    ""id"": ""1efa551e-430f-4579-a7e3-7148eff9c4af"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rudder"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""d0cfdac8-770a-4163-a974-63319c6cfddf"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rudder"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""81c95fd8-b1f9-4f95-85d6-3646204c3fb3"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rudder"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""cc794a7d-19b6-4732-b95a-f3718ffd31d9"",
                    ""path"": ""<Gamepad>/leftStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Rudder"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0a937849-c0bb-45cb-83e2-36a246a5f70d"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Gearshift_Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3848a4a9-2b03-4988-a1ee-9e9379bc6f7b"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Gearshift_Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""54c41145-4bae-4117-aa3d-a7b906006913"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Press(pressPoint=0.3)"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Gearshift_Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58d6038b-f8a5-4323-b29f-c798bd937367"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Gearshift_Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""954066ae-e6c1-4932-8cfb-6534e3e1f40a"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Gearshift_Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""32bed2a8-1e04-41da-87ef-84153686156c"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Gearshift_Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""298a8932-2885-4895-af3b-7160bfb391ef"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press(pressPoint=0.3)"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Gearshift_Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40cc7a52-5fef-4e2a-baa0-b5ab6423fa7a"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Gearshift_Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0f792113-0a0e-43b5-a697-0fa466b757b2"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7eab3873-33d1-4f00-b04d-edbc5881ec8b"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f6e6211f-cfce-4506-a0ba-080c155bc157"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7dac8464-9be5-4c87-822d-9e1bff16fcc2"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Siren"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a71e742-cc11-4b63-af5b-e59154e438df"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Siren"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""ProjectorControls"",
            ""id"": ""eca4835d-3648-43ec-843f-a9f0beb574c7"",
            ""actions"": [
                {
                    ""name"": ""Rotate"",
                    ""type"": ""Value"",
                    ""id"": ""9aa865a8-9007-4a10-a8fe-f2871e83af6a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""NormalizeVector2"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""fd136e21-6008-44f8-8761-b1a11c790874"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look_Left"",
                    ""type"": ""Button"",
                    ""id"": ""f39b81ca-038c-47f4-9924-7e5949760879"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look_Right"",
                    ""type"": ""Button"",
                    ""id"": ""3412fc79-4477-4171-b724-824841197f15"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""5c26bcf4-4fdd-4d51-b049-101f1b0e0819"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d57d436e-136d-43e0-99b1-4738d4853a5e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f60471cc-494b-410f-9448-b9f099360fd3"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""fab03b72-45c5-442e-a1e2-f91b6685cb84"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e8608e20-5351-43e7-8959-2c24f00c21b5"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ea5aadaa-b4f6-42fe-9442-fbbd1e2ec1b9"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2c083ed8-ce2f-45ce-a77c-a33e9eb336af"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4a398390-abe7-4f68-b54b-d4b83fb91d73"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1609b4b2-9587-4303-9ece-a3db42f15956"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""72416990-90b4-4c29-9b32-2ad0f15915d5"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look_Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d05d4172-60e1-49a4-a16a-bc31d0a28766"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look_Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1b7ffc36-7a5d-44dd-aa25-fd8502d3b49c"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Look_Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""afee327c-8707-4a26-ba82-537fd8e23cd7"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look_Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0f9fd103-d213-43a3-ad43-8114d120c951"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look_Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""30d6f447-c0b2-46bd-942d-be4f1a5e918a"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Look_Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard & Mouse"",
            ""bindingGroup"": ""Keyboard & Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // SailorControls
            m_SailorControls = asset.FindActionMap("SailorControls", throwIfNotFound: true);
            m_SailorControls_Walk = m_SailorControls.FindAction("Walk", throwIfNotFound: true);
            m_SailorControls_Look = m_SailorControls.FindAction("Look", throwIfNotFound: true);
            m_SailorControls_Interact = m_SailorControls.FindAction("Interact", throwIfNotFound: true);
            m_SailorControls_AutoLook = m_SailorControls.FindAction("AutoLook", throwIfNotFound: true);
            m_SailorControls_Pause = m_SailorControls.FindAction("Pause", throwIfNotFound: true);
            // BoatControls
            m_BoatControls = asset.FindActionMap("BoatControls", throwIfNotFound: true);
            m_BoatControls_Rudder = m_BoatControls.FindAction("Rudder", throwIfNotFound: true);
            m_BoatControls_Gearshift_Up = m_BoatControls.FindAction("Gearshift_Up", throwIfNotFound: true);
            m_BoatControls_Gearshift_Down = m_BoatControls.FindAction("Gearshift_Down", throwIfNotFound: true);
            m_BoatControls_Exit = m_BoatControls.FindAction("Exit", throwIfNotFound: true);
            m_BoatControls_Siren = m_BoatControls.FindAction("Siren", throwIfNotFound: true);
            // ProjectorControls
            m_ProjectorControls = asset.FindActionMap("ProjectorControls", throwIfNotFound: true);
            m_ProjectorControls_Rotate = m_ProjectorControls.FindAction("Rotate", throwIfNotFound: true);
            m_ProjectorControls_Exit = m_ProjectorControls.FindAction("Exit", throwIfNotFound: true);
            m_ProjectorControls_Look_Left = m_ProjectorControls.FindAction("Look_Left", throwIfNotFound: true);
            m_ProjectorControls_Look_Right = m_ProjectorControls.FindAction("Look_Right", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // SailorControls
        private readonly InputActionMap m_SailorControls;
        private ISailorControlsActions m_SailorControlsActionsCallbackInterface;
        private readonly InputAction m_SailorControls_Walk;
        private readonly InputAction m_SailorControls_Look;
        private readonly InputAction m_SailorControls_Interact;
        private readonly InputAction m_SailorControls_AutoLook;
        private readonly InputAction m_SailorControls_Pause;
        public struct SailorControlsActions
        {
            private @PlayerActions m_Wrapper;
            public SailorControlsActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Walk => m_Wrapper.m_SailorControls_Walk;
            public InputAction @Look => m_Wrapper.m_SailorControls_Look;
            public InputAction @Interact => m_Wrapper.m_SailorControls_Interact;
            public InputAction @AutoLook => m_Wrapper.m_SailorControls_AutoLook;
            public InputAction @Pause => m_Wrapper.m_SailorControls_Pause;
            public InputActionMap Get() { return m_Wrapper.m_SailorControls; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(SailorControlsActions set) { return set.Get(); }
            public void SetCallbacks(ISailorControlsActions instance)
            {
                if (m_Wrapper.m_SailorControlsActionsCallbackInterface != null)
                {
                    @Walk.started -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnWalk;
                    @Walk.performed -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnWalk;
                    @Walk.canceled -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnWalk;
                    @Look.started -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnLook;
                    @Look.performed -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnLook;
                    @Look.canceled -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnLook;
                    @Interact.started -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnInteract;
                    @Interact.performed -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnInteract;
                    @Interact.canceled -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnInteract;
                    @AutoLook.started -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnAutoLook;
                    @AutoLook.performed -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnAutoLook;
                    @AutoLook.canceled -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnAutoLook;
                    @Pause.started -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnPause;
                    @Pause.performed -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnPause;
                    @Pause.canceled -= m_Wrapper.m_SailorControlsActionsCallbackInterface.OnPause;
                }
                m_Wrapper.m_SailorControlsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Walk.started += instance.OnWalk;
                    @Walk.performed += instance.OnWalk;
                    @Walk.canceled += instance.OnWalk;
                    @Look.started += instance.OnLook;
                    @Look.performed += instance.OnLook;
                    @Look.canceled += instance.OnLook;
                    @Interact.started += instance.OnInteract;
                    @Interact.performed += instance.OnInteract;
                    @Interact.canceled += instance.OnInteract;
                    @AutoLook.started += instance.OnAutoLook;
                    @AutoLook.performed += instance.OnAutoLook;
                    @AutoLook.canceled += instance.OnAutoLook;
                    @Pause.started += instance.OnPause;
                    @Pause.performed += instance.OnPause;
                    @Pause.canceled += instance.OnPause;
                }
            }
        }
        public SailorControlsActions @SailorControls => new SailorControlsActions(this);

        // BoatControls
        private readonly InputActionMap m_BoatControls;
        private IBoatControlsActions m_BoatControlsActionsCallbackInterface;
        private readonly InputAction m_BoatControls_Rudder;
        private readonly InputAction m_BoatControls_Gearshift_Up;
        private readonly InputAction m_BoatControls_Gearshift_Down;
        private readonly InputAction m_BoatControls_Exit;
        private readonly InputAction m_BoatControls_Siren;
        public struct BoatControlsActions
        {
            private @PlayerActions m_Wrapper;
            public BoatControlsActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Rudder => m_Wrapper.m_BoatControls_Rudder;
            public InputAction @Gearshift_Up => m_Wrapper.m_BoatControls_Gearshift_Up;
            public InputAction @Gearshift_Down => m_Wrapper.m_BoatControls_Gearshift_Down;
            public InputAction @Exit => m_Wrapper.m_BoatControls_Exit;
            public InputAction @Siren => m_Wrapper.m_BoatControls_Siren;
            public InputActionMap Get() { return m_Wrapper.m_BoatControls; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(BoatControlsActions set) { return set.Get(); }
            public void SetCallbacks(IBoatControlsActions instance)
            {
                if (m_Wrapper.m_BoatControlsActionsCallbackInterface != null)
                {
                    @Rudder.started -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnRudder;
                    @Rudder.performed -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnRudder;
                    @Rudder.canceled -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnRudder;
                    @Gearshift_Up.started -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Up;
                    @Gearshift_Up.performed -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Up;
                    @Gearshift_Up.canceled -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Up;
                    @Gearshift_Down.started -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Down;
                    @Gearshift_Down.performed -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Down;
                    @Gearshift_Down.canceled -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnGearshift_Down;
                    @Exit.started -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnExit;
                    @Exit.performed -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnExit;
                    @Exit.canceled -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnExit;
                    @Siren.started -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnSiren;
                    @Siren.performed -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnSiren;
                    @Siren.canceled -= m_Wrapper.m_BoatControlsActionsCallbackInterface.OnSiren;
                }
                m_Wrapper.m_BoatControlsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Rudder.started += instance.OnRudder;
                    @Rudder.performed += instance.OnRudder;
                    @Rudder.canceled += instance.OnRudder;
                    @Gearshift_Up.started += instance.OnGearshift_Up;
                    @Gearshift_Up.performed += instance.OnGearshift_Up;
                    @Gearshift_Up.canceled += instance.OnGearshift_Up;
                    @Gearshift_Down.started += instance.OnGearshift_Down;
                    @Gearshift_Down.performed += instance.OnGearshift_Down;
                    @Gearshift_Down.canceled += instance.OnGearshift_Down;
                    @Exit.started += instance.OnExit;
                    @Exit.performed += instance.OnExit;
                    @Exit.canceled += instance.OnExit;
                    @Siren.started += instance.OnSiren;
                    @Siren.performed += instance.OnSiren;
                    @Siren.canceled += instance.OnSiren;
                }
            }
        }
        public BoatControlsActions @BoatControls => new BoatControlsActions(this);

        // ProjectorControls
        private readonly InputActionMap m_ProjectorControls;
        private IProjectorControlsActions m_ProjectorControlsActionsCallbackInterface;
        private readonly InputAction m_ProjectorControls_Rotate;
        private readonly InputAction m_ProjectorControls_Exit;
        private readonly InputAction m_ProjectorControls_Look_Left;
        private readonly InputAction m_ProjectorControls_Look_Right;
        public struct ProjectorControlsActions
        {
            private @PlayerActions m_Wrapper;
            public ProjectorControlsActions(@PlayerActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Rotate => m_Wrapper.m_ProjectorControls_Rotate;
            public InputAction @Exit => m_Wrapper.m_ProjectorControls_Exit;
            public InputAction @Look_Left => m_Wrapper.m_ProjectorControls_Look_Left;
            public InputAction @Look_Right => m_Wrapper.m_ProjectorControls_Look_Right;
            public InputActionMap Get() { return m_Wrapper.m_ProjectorControls; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(ProjectorControlsActions set) { return set.Get(); }
            public void SetCallbacks(IProjectorControlsActions instance)
            {
                if (m_Wrapper.m_ProjectorControlsActionsCallbackInterface != null)
                {
                    @Rotate.started -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnRotate;
                    @Rotate.performed -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnRotate;
                    @Rotate.canceled -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnRotate;
                    @Exit.started -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnExit;
                    @Exit.performed -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnExit;
                    @Exit.canceled -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnExit;
                    @Look_Left.started -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Left;
                    @Look_Left.performed -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Left;
                    @Look_Left.canceled -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Left;
                    @Look_Right.started -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Right;
                    @Look_Right.performed -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Right;
                    @Look_Right.canceled -= m_Wrapper.m_ProjectorControlsActionsCallbackInterface.OnLook_Right;
                }
                m_Wrapper.m_ProjectorControlsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Rotate.started += instance.OnRotate;
                    @Rotate.performed += instance.OnRotate;
                    @Rotate.canceled += instance.OnRotate;
                    @Exit.started += instance.OnExit;
                    @Exit.performed += instance.OnExit;
                    @Exit.canceled += instance.OnExit;
                    @Look_Left.started += instance.OnLook_Left;
                    @Look_Left.performed += instance.OnLook_Left;
                    @Look_Left.canceled += instance.OnLook_Left;
                    @Look_Right.started += instance.OnLook_Right;
                    @Look_Right.performed += instance.OnLook_Right;
                    @Look_Right.canceled += instance.OnLook_Right;
                }
            }
        }
        public ProjectorControlsActions @ProjectorControls => new ProjectorControlsActions(this);
        private int m_KeyboardMouseSchemeIndex = -1;
        public InputControlScheme KeyboardMouseScheme
        {
            get
            {
                if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard & Mouse");
                return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
            }
        }
        private int m_GamepadSchemeIndex = -1;
        public InputControlScheme GamepadScheme
        {
            get
            {
                if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
                return asset.controlSchemes[m_GamepadSchemeIndex];
            }
        }
        public interface ISailorControlsActions
        {
            void OnWalk(InputAction.CallbackContext context);
            void OnLook(InputAction.CallbackContext context);
            void OnInteract(InputAction.CallbackContext context);
            void OnAutoLook(InputAction.CallbackContext context);
            void OnPause(InputAction.CallbackContext context);
        }
        public interface IBoatControlsActions
        {
            void OnRudder(InputAction.CallbackContext context);
            void OnGearshift_Up(InputAction.CallbackContext context);
            void OnGearshift_Down(InputAction.CallbackContext context);
            void OnExit(InputAction.CallbackContext context);
            void OnSiren(InputAction.CallbackContext context);
        }
        public interface IProjectorControlsActions
        {
            void OnRotate(InputAction.CallbackContext context);
            void OnExit(InputAction.CallbackContext context);
            void OnLook_Left(InputAction.CallbackContext context);
            void OnLook_Right(InputAction.CallbackContext context);
        }
    }
}
