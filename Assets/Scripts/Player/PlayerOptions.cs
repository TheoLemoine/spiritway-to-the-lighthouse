﻿using UnityEngine;

namespace Spiritway.Player
{
    
    /**
     * Scriptable Object providing information on personal user settings.
     * Can be changed at runtime. Provide asset to a behaviour by assigning it to a serialized field.
     */
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Spiritway/Settings/PlayerSettings", order = 0)]
    public class PlayerOptions : ScriptableObject
    {
        [Header("Sensitivity")] 
        public float lookSensitivity = 1;

    }
}