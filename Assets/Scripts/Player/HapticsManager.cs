﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritway.Player
{
    [CreateAssetMenu(fileName = "HapticsManager", menuName = "Spiritway/Managers/Haptics Manager", order = 0)]
    public class HapticsManager : ScriptableObject
    {
        float _lowFrequency;
        float _highFrequency;

        void OnEnable()
        {
            _lowFrequency = 0f;
            _highFrequency = 0f;
        }

        public ContinuousHaptic MakeContinuousHaptic(float lowFreq, float highFreq)
        {
            return new ContinuousHaptic(this, lowFreq, highFreq);
        }

        public IEnumerator AddTemporaryHaptic(float lowFreq, float highFreq, float duration)
        {
            _lowFrequency += lowFreq;
            _highFrequency += highFreq;
            UpdateHaptics();

            yield return new WaitForSeconds(duration);
            
            _lowFrequency -= lowFreq;
            _highFrequency -= highFreq;
            UpdateHaptics();
        }

        public void AddHaptic(float lowFreq, float highFreq)
        {
            _lowFrequency += lowFreq;
            _highFrequency += highFreq;
            UpdateHaptics();
        }

        void UpdateHaptics()
        {
            Gamepad.current?.SetMotorSpeeds(
                Mathf.Clamp(_lowFrequency, 0, 1), 
                Mathf.Clamp(_highFrequency, 0, 1));
        }

        /**
         * Struct that keeps track of continuous haptics added.
         */
        public struct ContinuousHaptic
        {
            public bool Active { get; private set; }
            HapticsManager _manager;
            float _lowFrequency;
            float _highFrequency;

            public ContinuousHaptic(HapticsManager manager, float low, float high)
            {
                _manager = manager;
                _lowFrequency = low;
                _highFrequency = high;
                Active = false;
            }

            public void Activate()
            {
                if (Active) return;
                
                Active = true;
                _manager.AddHaptic(_lowFrequency, _highFrequency);
            }

            public void Deactivate()
            {
                if (!Active) return;
                
                Active = false;
                _manager.AddHaptic(-_lowFrequency, -_highFrequency);
            }
        }

    }
}