﻿using UnityEngine;

namespace Spiritway.Levels
{
    /**
     * Directly loads the next level at start (used for first level)
     */
    public class LoadNextLevelDirect : ALoadNextLevel
    {
        [SerializeField] Transform nextLevelTransform;
        
        protected override void Start()
        {
            base.Start();
            StartCoroutine(SmokeTransitionAndLoad(nextLevelTransform.position, nextLevelTransform.rotation));
        }
    }
}