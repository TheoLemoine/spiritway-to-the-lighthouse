﻿using UnityEngine;

namespace Spiritway.Levels
{
    [RequireComponent(typeof(Collider))]
    public class LoadNextLevelOnTrigger : ALoadNextLevel
    {
        [SerializeField] Transform nextLevelTransform;

        bool _active = true;
        
        void OnTriggerEnter(Collider other)
        {
            if(!_active) return;
            
            StartCoroutine(SmokeTransitionAndLoad(nextLevelTransform.position, nextLevelTransform.rotation));
            _active = false;
        }
    }
}