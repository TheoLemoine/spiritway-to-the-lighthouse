﻿using System.Collections.Generic;
using UnityEngine;

namespace Spiritway.Levels
{
    [CreateAssetMenu(fileName = "LevelManager", menuName = "Spiritway/Managers/Level Manager", order = 0)]
    public class LevelManager : ScriptableObject
    {
        public List<GameObject> LoadedLevels { get; private set; }

        void OnEnable()
        {
            LoadedLevels = new List<GameObject>();
        }

        public void LoadNextLevel(GameObject levelPrefab, Vector3 levelPos, Quaternion levelRot)
        {
            var newLevel = Instantiate(levelPrefab, levelPos, levelRot);
            LoadedLevels.Add(newLevel);
        }
    }
}