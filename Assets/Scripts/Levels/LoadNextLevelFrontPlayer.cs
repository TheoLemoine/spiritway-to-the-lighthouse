﻿using Spiritway.Boat;
using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Levels
{
    public class LoadNextLevelFrontPlayer : ALoadNextLevel
    {
        [SerializeField, TagSelector] string boatTag;

        [SerializeField] float triggerDistance;
        [SerializeField] float loadDistance;
        [SerializeField, Range(0f, 180f)] float maxSpawnAngle = 90f;
        [SerializeField, Range(0f, 180f)] float maxDetectAngle = 90f;

        Transform _transform;
        Transform _boatTransform;

        bool _active = true;
        
        protected override void Start()
        {
            _transform = GetComponent<Transform>();
            _boatTransform = GameObject.FindWithTag(boatTag).transform;
            
            base.Start();
        }

        void Update()
        {
            if(!_active) return;
            
            var toBoat = _boatTransform.position - _transform.position;

            if (toBoat.sqrMagnitude >= triggerDistance * triggerDistance)
            {
                var loadDir = toBoat.normalized;
                var loadAngle = Vector3.SignedAngle(transform.forward, loadDir, Vector3.up);
                
                if(Mathf.Abs(loadAngle) > maxDetectAngle) return;
                
                if (Mathf.Abs(loadAngle) > maxSpawnAngle)
                {
                    loadDir = Quaternion.Euler(0, maxSpawnAngle * Mathf.Sign(loadAngle), 0) * Vector3.forward;
                }
                
                var loadPos = _transform.position + loadDir * loadDistance;

                var loadRot = Quaternion.LookRotation(loadDir, Vector3.up);

                StartCoroutine(SmokeTransitionAndLoad(loadPos, loadRot));
                _active = false;
            }
        }

#if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            var pos = transform.position;
            var forward = transform.forward;
            
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(pos, triggerDistance);
            
            Gizmos.DrawLine(pos, pos + (Quaternion.Euler(0, maxDetectAngle, 0) * forward * triggerDistance));
            Gizmos.DrawLine(pos, pos + (Quaternion.Euler(0, -maxDetectAngle, 0) * forward * triggerDistance));

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(pos, loadDistance);
            
            Gizmos.DrawLine(pos, pos + (Quaternion.Euler(0, maxSpawnAngle, 0) * forward * loadDistance));
            Gizmos.DrawLine(pos, pos + (Quaternion.Euler(0, -maxSpawnAngle, 0) * forward * loadDistance));
        }
#endif
        
    }
}