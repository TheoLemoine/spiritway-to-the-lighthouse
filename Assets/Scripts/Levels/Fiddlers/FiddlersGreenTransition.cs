﻿using System.Collections;
using System.Linq;
using Spiritway.Boat;
using Spiritway.Dialogue;
using Spiritway.Sound;
using Spiritway.UI;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Spiritway.Levels.Fiddlers
{
    public class FiddlersGreenTransition : ATriggerOnce
    {
        [Header("Transition")] 
        [SerializeField] float beforeTransitionDuration;
        [SerializeField] float transitionDuration;
        [SerializeField, TagSelector] string boatTag;
        [SerializeField, TagSelector] string sailorTag;
        [SerializeField, TagSelector] string levelTag;
        [SerializeField, TagSelector] string oceanTag;
        [SerializeField] Volume transitionVolume;
        [SerializeField] GameObject fiddlersGreenPrefab;

        [Header("Sound")] 
        [SerializeField] SoundManager soundManager;
        [SerializeField] AK.Wwise.State oceanState;
        [SerializeField] AK.Wwise.State fiddlersMusicState;
        [SerializeField] AK.Wwise.State creditsMusicState;

        [Header("After Transition")] 
        [SerializeField] float afterTransitionTime = 45f;
        [SerializeField] float boatSpeedAfterTransition = 10f;

        [Header("Credits")] 
        [SerializeField] UIManager uiManager;
        [SerializeField] float timeFadeToBlack;
        [SerializeField] DialogueLine endLine;
        [SerializeField] VisualTreeAsset creditsHud;
        [SerializeField] float beforeCreditsTime;
        [SerializeField] float creditsFadeInTime;
        [SerializeField] float beforeCreditSlideTime;
        [SerializeField] float creditsSlideAmount = 5.5f;
        [SerializeField] float creditsSlideTime;
        [SerializeField] float afterCredits;
 
        protected override void OnTrigger()
        {
            StartCoroutine(Transition());
        }

        /**
         * This is the scripted big timeline of what happen in the end
         */
        IEnumerator Transition()
        {
            // --- Music in
            soundManager.SetCurrentState(SoundManager.StateTypes.Music, fiddlersMusicState);
            
            // --- Wait a bit
            yield return new WaitForSeconds(beforeTransitionDuration);

            // --- Fog in
            for (float elapsed = 0; elapsed < transitionDuration / 2; elapsed += Time.deltaTime)
            {
                var progress = elapsed / (transitionDuration / 2);
                transitionVolume.weight = Mathf.SmoothStep(0, 1, progress);

                yield return null;
            }
            
            // --- Remove all loaded levels 
            foreach (var level in GameObject.FindGameObjectsWithTag(levelTag))
            {
                Destroy(level);
            }

            // --- Remove ocean
            Destroy(GameObject.FindWithTag(oceanTag));
            soundManager.SetCurrentState(SoundManager.StateTypes.Ocean, oceanState);

            // --- Stop boat and keep player from interacting with anything
            var boat = GameObject.FindWithTag(boatTag);
            foreach (var interactable in boat.GetComponentsInChildren<ABoatLockedInteractable>())
            {
                interactable.DeactivateInteraction();
            }

            var boatEngine = boat.GetComponent<BoatEngine>();
            boatEngine.Gear = 0;
            
            var boatRb = boat.GetComponent<Rigidbody>();
            boatRb.isKinematic = true;
            boatRb.velocity = Vector3.zero;

            var sailor = GameObject.FindWithTag(sailorTag);
            var sailorRb = sailor.GetComponent<Rigidbody>();
            sailorRb.velocity = Vector3.zero;

            // --- Add fiddler's green prefab
            Instantiate(fiddlersGreenPrefab, boat.transform.position, boat.transform.rotation);
            
            // --- Set boat to move slowly forward
            StartCoroutine(SetVelocityContinuous(boatRb, Vector3.forward * boatSpeedAfterTransition));

            // --- Fog out
            for (float elapsed = 0; elapsed < transitionDuration / 2; elapsed += Time.deltaTime)
            {
                var progress = elapsed / (transitionDuration / 2);
                transitionVolume.weight = Mathf.SmoothStep(0, 1, 1 - progress);
                
                yield return null;
            }

            // --- Let player enjoy fiddler's green a bit
            yield return new WaitForSeconds(afterTransitionTime);
            
            // --- Screen goes black
            yield return uiManager.FadeToBlack(timeFadeToBlack);
            
            // --- Sailor says a final word
            yield return soundManager.PlayLine(endLine, gameObject);
            
            // --- Wait a bit before credits
            yield return new WaitForSeconds(beforeCreditsTime);
            
            // --- Credits music in
            soundManager.SetCurrentState(SoundManager.StateTypes.Music, creditsMusicState);
            
            // --- Add credits in (smoothly)
            var credits = creditsHud.Instantiate().Children().First();
            var creditsImage = credits.Q("credits_image");
            uiManager.AddScreenToHud(credits);
            
            yield return uiManager.FadeFromBlack(creditsFadeInTime, false);

            // --- Hold the logo for a bit
            yield return new WaitForSeconds(beforeCreditSlideTime);

            // --- Slide credits
            var targetTopValue = -creditsSlideAmount * Screen.height;
            
            for (float elapsed = 0; elapsed < creditsSlideTime; elapsed += Time.deltaTime)
            {
                var progress = elapsed / creditsSlideTime;
                creditsImage.style.top = Mathf.Lerp(0, targetTopValue, progress);
                
                yield return null;
            }
            
            creditsImage.style.top = targetTopValue;
            
            // --- Credits out (smoothly)
            yield return uiManager.FadeToBlack(creditsFadeInTime, false);

            // --- Let player enjoy a bit of nothing
            yield return new WaitForSeconds(afterCredits);
            
            // --- Back to main menu
            SceneManager.LoadScene("MainMenu");
        }

        IEnumerator SetVelocityContinuous(Rigidbody rb, Vector3 velocity)
        {
            for (;;)
            {
                rb.MovePosition(rb.transform.TransformPoint(velocity * Time.fixedDeltaTime));
                yield return new WaitForFixedUpdate();
            }
        }
    }
}