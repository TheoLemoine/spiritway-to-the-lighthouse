﻿using System;
using System.Collections;
using Spiritway.Utils;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace Spiritway.Levels
{
    public class ALoadNextLevel : MonoBehaviour
    {
        [Header("Fog Transition")] 
        [SerializeField] bool doTransition = true;
        [SerializeField] float transitionDuration = 5;
        [SerializeField] Volume transitionVolume;
        [SerializeField, TagSelector] string lighthouseTag;
        
        [Header("Level Load")]
        [SerializeField] LevelManager manager;
        [SerializeField] GameObject nextLevelPrefab;

        Fog _fog;
        Transform _lighthouseTransform;

        protected virtual void Start()
        {
            if (doTransition)
            {
                _lighthouseTransform = GameObject
                    .FindWithTag(lighthouseTag)
                    .transform;
            }
        }

        protected IEnumerator SmokeTransitionAndLoad(Vector3 loadPos, Quaternion loadRot)
        {
            if (!doTransition)
            {
                manager.LoadNextLevel(nextLevelPrefab, loadPos, loadRot);
                yield break;
            }
            
            transitionVolume.weight = Mathf.SmoothStep(0, 1, 1);
            
            // fog in
            for (float elapsed = 0; elapsed < transitionDuration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / transitionDuration;
                transitionVolume.weight = Mathf.SmoothStep(0, 1, progress);
                
                yield return null;
            }
            
            // load level
            manager.LoadNextLevel(nextLevelPrefab, loadPos, loadRot);
            // orient lighthouse
            _lighthouseTransform.rotation = loadRot;
            
            // fog out 
            for (float elapsed = 0; elapsed < transitionDuration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / transitionDuration;
                transitionVolume.weight = Mathf.SmoothStep(0, 1, 1 - progress);
                
                yield return null;
            }
            
            transitionVolume.weight = Mathf.SmoothStep(0, 1, 0);
        }
    }
}