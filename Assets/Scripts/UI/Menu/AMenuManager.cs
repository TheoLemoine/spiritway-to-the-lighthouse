﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Spiritway.UI.Menu
{
    public abstract class AMenuManager : MonoBehaviour
    {
        [SerializeField] Image blackPanel;
        
        protected bool IsLoadingScene;
        
        protected IEnumerator FadeToBlack(float duration, Action onBlack)
        {
            for (float elapsed = 0; elapsed < duration; elapsed += Time.unscaledDeltaTime)
            {
                var progress = elapsed / duration;
                blackPanel.color = new Color(0, 0, 0, progress);
                
                AkSoundEngine.SetRTPCValue("Amb_RTPC", 1 - progress);
                
                yield return null;
            }
            
            blackPanel.color = new Color(0, 0, 0, 1);
            AkSoundEngine.SetRTPCValue("Amb_RTPC", 0);

            yield return null;

            onBlack();
        }

        protected IEnumerator FadeFromBlack(float duration, Action onFinished)
        {
            for (float elapsed = 0; elapsed < duration; elapsed += Time.unscaledDeltaTime)
            {
                var progress = elapsed / duration;
                blackPanel.color = new Color(0, 0, 0, 1 - progress);
                
                AkSoundEngine.SetRTPCValue("Amb_RTPC", progress);
                
                yield return null;
            }
            
            blackPanel.color = new Color(0, 0, 0, 0);
            AkSoundEngine.SetRTPCValue("Amb_RTPC", 1);

            yield return null;

            onFinished();
        }
    }
}