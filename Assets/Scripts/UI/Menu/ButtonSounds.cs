﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Spiritway.UI.Menu
{
    public class ButtonSounds : MonoBehaviour, ISelectHandler, ISubmitHandler
    {
        [SerializeField] AK.Wwise.Event buttonClick;
        [SerializeField] AK.Wwise.Event focusSound;

        public void OnSelect(BaseEventData eventData)
        {
            focusSound.Post(gameObject);
        }

        public void OnSubmit(BaseEventData eventData)
        {
            buttonClick.Post(gameObject);
        }
    }
}