﻿using System;
using Spiritway.Boat;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Spiritway.UI.Menu
{
    public class MainMenu : AMenuManager
    {
        [SerializeField] BoatEngine boatEngine;
        [SerializeField] Selectable defaultSelected;
        [SerializeField] AK.Wwise.State menuOceanSound;
        [SerializeField] AK.Wwise.Event playSound;

        void Awake()
        {
            // hide cursor, and prevent it from leaving window
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            
            menuOceanSound.SetValue();
        }

        void Start()
        {
            StartCoroutine(FadeFromBlack(1f, () =>
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(defaultSelected.gameObject);
            }));
        }

        public void LoadMainScene()
        {
            if (!IsLoadingScene)
            {
                playSound.Post(gameObject);
                IsLoadingScene = true;
                boatEngine.Gear = 2;
                StartCoroutine(FadeToBlack(3f, () => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1)));
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}