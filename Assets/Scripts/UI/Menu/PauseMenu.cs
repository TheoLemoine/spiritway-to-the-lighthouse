﻿using System;
using System.Collections;
using Spiritway.Player;
using Spiritway.Respawn;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Spiritway.UI.Menu
{
    public class PauseMenu : AMenuManager
    {
        [SerializeField] string mainMenuSceneName = "MainMenu";
        [SerializeField] Selectable defaultSelected;
        [SerializeField] GameObject canvas;
        [SerializeField] RespawnManager respawnManager;
        
        PlayerActions _actions;

        void Start()
        {
            canvas.SetActive(false);
            
            _actions = new PlayerActions();
            _actions.SailorControls.Pause.performed += OnPause;
            _actions.Enable();
        }

        void OnDestroy()
        {
            _actions.Disable();
        }

        void OnPause(InputAction.CallbackContext context)
        {
            if (canvas.activeSelf)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        public void Pause()
        {
            AkSoundEngine.SetRTPCValue("Menu_RTPC", 1);
            canvas.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(defaultSelected.gameObject);
            Time.timeScale = 0;
        }

        public void Resume()
        {
            AkSoundEngine.SetRTPCValue("Menu_RTPC", 0);
            canvas.SetActive(false);
            Time.timeScale = 1;
        }

        public void LoadMainMenu()
        {
            if (!IsLoadingScene)
            {
                IsLoadingScene = true;
                StartCoroutine(FadeToBlack(1f, () =>
                {
                    Time.timeScale = 1;
                    AkSoundEngine.SetRTPCValue("Menu_RTPC", 0);
                    SceneManager.LoadScene(mainMenuSceneName);
                }));
            }
        }

        public void ReturnToLastCheckpoint()
        {
            StartCoroutine(FadeToBlack(1f, () =>
            {
                respawnManager.TriggerRespawn();
                Resume();

                StartCoroutine(FadeFromBlack(1f, () => {}));
            }));
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}