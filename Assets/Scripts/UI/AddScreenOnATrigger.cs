﻿using System.Linq;
using Spiritway.Utils;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.UI
{
    public class AddScreenOnATrigger : ATriggerOnce
    {
        [SerializeField] UIManager manager;
        [SerializeField] VisualTreeAsset screen;

        protected override void OnTrigger()
        {
            manager.AddScreenToHud(screen.Instantiate().Children().First());
        }
    }
}