﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.UI
{
    public class RegisterHUD : MonoBehaviour
    {
        [SerializeField] UIManager manager;
        
        void Awake()
        {
            var document = GetComponent<UIDocument>();
            manager.HudRoot = document.rootVisualElement;

            StartCoroutine(manager.SetSubtitle("", 0));
        }
    }
}