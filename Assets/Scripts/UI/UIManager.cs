﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.UI
{
    /**
     * This contains access and manage UI elements globally 
     */
    [CreateAssetMenu(fileName = "UIManager", menuName = "Spiritway/Managers/UI Manager", order = 0)]
    public class UIManager : ScriptableObject
    {
        public UIDocument HudDocument { get; set; }
        public VisualElement HudRoot { get; set; }

        public void AddScreenToHud(VisualElement screen)
        {
            HudRoot.Q("added_hud_root").Add(screen);
        }

        public void SetBlackPanelOpacity(float opacity)
        {
            var panel = HudRoot.Q<VisualElement>("black_panel");
            panel.style.opacity = opacity;
            
            AkSoundEngine.SetRTPCValue("Amb_RTPC", 1 - opacity);
        }

        public IEnumerator FadeToBlack(float seconds, bool fadeSound = true)
        {
            var panel = HudRoot.Q<VisualElement>("black_panel");

            for (float elapsed = 0; elapsed < seconds; elapsed += Time.deltaTime)
            {
                var progress = elapsed / seconds;

                panel.style.opacity = progress;
                if(fadeSound) AkSoundEngine.SetRTPCValue("Amb_RTPC", 1 - progress);
                
                yield return null;
            }
            
            panel.style.opacity = 1;
            if(fadeSound) AkSoundEngine.SetRTPCValue("Amb_RTPC", 0);
        }

        public IEnumerator FadeFromBlack(float seconds, bool fadeSound = true)
        {
            var panel = HudRoot.Q<VisualElement>("black_panel");

            for (float elapsed = 0; elapsed < seconds; elapsed += Time.deltaTime)
            {
                var progress = elapsed / seconds;

                panel.style.opacity = 1 - progress;
                if(fadeSound) AkSoundEngine.SetRTPCValue("Amb_RTPC", progress);
                
                yield return null;
            }
            
            panel.style.opacity = 0;
            if(fadeSound) AkSoundEngine.SetRTPCValue("Amb_RTPC", 1);
        }

        public IEnumerator SetSubtitle(string line, float duration)
        {
            var subtitleElem = HudRoot.Q<Label>("subtitle_box");
            subtitleElem.text = line;

            yield return new WaitForSeconds(duration);
            
            subtitleElem.text = "";
        }
    }
}