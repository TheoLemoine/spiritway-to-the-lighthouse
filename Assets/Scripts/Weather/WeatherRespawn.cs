﻿using Spiritway.Respawn;
using UnityEngine;

namespace Spiritway.Weather
{
    public class WeatherRespawn : ARespawnableBehaviour
    {
        [SerializeField] GlobalWeatherManager weatherManager;
        
        int _savedOceanLevel;
        int _savedFogLevel;
        int _savedRainLevel;
        int _savedSkyLevel;

        public override void Save()
        {
            _savedOceanLevel = weatherManager.Ocean.GetLevel();
            _savedFogLevel = weatherManager.Fog.GetLevel();
            _savedRainLevel = weatherManager.Rain.GetLevel();
            _savedSkyLevel = weatherManager.Sky.GetLevel();
        }

        public override void Respawn()
        {
            weatherManager.SetWeatherLevels(_savedOceanLevel, _savedFogLevel, _savedRainLevel, _savedSkyLevel, 0.1f);
        }
    }
}