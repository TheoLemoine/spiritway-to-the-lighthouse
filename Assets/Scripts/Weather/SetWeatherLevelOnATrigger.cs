﻿using System;
using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Weather
{
    public class SetWeatherLevelOnATrigger : ATriggerOnce
    {
        [SerializeField] GlobalWeatherManager manager;

        [SerializeField] float changeDuration;
        [SerializeField] int ocean;
        [SerializeField] int fog;
        [SerializeField] int rain;
        [SerializeField] int sky;

        protected override void OnTrigger()
        {
            manager.SetWeatherLevels(ocean, fog, rain, sky, changeDuration);
        }
    }
}