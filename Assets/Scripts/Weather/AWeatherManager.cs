﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Weather
{
    public abstract class AWeatherManager<TLevel, TInterpolator> : MonoBehaviour 
        where TLevel: ScriptableObject 
        where TInterpolator : AWeatherLevelInterpolator<TLevel>, new()
    {
        [SerializeField] protected GlobalWeatherManager manager;
        [SerializeField] TLevel[] levels;
        [SerializeField] int defaultLevel;

        int _currentLevelIndex;
        protected TLevel CurrentLevel { get; private set; }

        void Awake()
        {
            CurrentLevel = ScriptableObject.CreateInstance<TLevel>();
            SetupWeatherLevel();
            SetLevel(defaultLevel, 0.1f);
        }

        public void SetLevel(int level, float duration)
        {
            var previousLevel = _currentLevelIndex;
            _currentLevelIndex = level;

            var interpolator = new TInterpolator();
            interpolator.Init(levels[previousLevel], levels[_currentLevelIndex], CurrentLevel, duration);

            StartCoroutine(interpolator.RunInterpolation(UpdateWeather));
        }

        public int GetLevel()
        {
            return _currentLevelIndex;
        }

        protected abstract void SetupWeatherLevel();
        
        protected abstract void UpdateWeather();
    }
}