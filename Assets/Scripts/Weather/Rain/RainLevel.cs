﻿using AK.Wwise;
using UnityEngine;

namespace Spiritway.Weather.Rain
{
    [CreateAssetMenu(fileName = "RainLevel", menuName = "Spiritway/Weather/RainLevel", order = 0)]
    public class RainLevel : ScriptableObject
    {
        [Header("Rain FX")] 
        [Range(0f, 1f)] public float intensity = 0f;

        [Header("Rain on screen")] 
        public float dropsPerSeconds = 4f;
    }
}