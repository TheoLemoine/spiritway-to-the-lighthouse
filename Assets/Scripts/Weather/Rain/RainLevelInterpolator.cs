﻿using UnityEngine;

namespace Spiritway.Weather.Rain
{
    public class RainLevelInterpolator : AWeatherLevelInterpolator<RainLevel>
    {
        protected override void BeforeInterpolate()
        {
            WeatherLevel.dropsPerSeconds = To.dropsPerSeconds;
        }

        protected override void Interpolate(float alpha)
        {
            WeatherLevel.intensity = Mathf.Lerp(From.intensity, To.intensity, alpha); 
        }
    }
}