﻿using UnityEngine;
using UnityEngine.VFX;

namespace Spiritway.Weather.Rain
{
    public class RainManager : AWeatherManager<RainLevel, RainLevelInterpolator>
    {
        [SerializeField] public VisualEffect rainVFX;
        [SerializeField] public RainDropsGenerator raindropsGenerator;

        protected override void SetupWeatherLevel()
        {
            manager.Rain = this;
        }

        protected override void UpdateWeather()
        {
            rainVFX.SetFloat("Intensity", CurrentLevel.intensity);
            raindropsGenerator.RainDropsPerSeconds = CurrentLevel.dropsPerSeconds;
        }
    }
}