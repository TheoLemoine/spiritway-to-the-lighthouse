﻿using System;
using UnityEngine;
using UnityEngine.VFX;

namespace Spiritway.Weather.Rain
{
    public class SetExcludeRainSphere : MonoBehaviour
    {
        [SerializeField] GlobalWeatherManager weatherManager;
        [SerializeField] Vector3 sphereCenter;
        [SerializeField] float sphereRadius;

        VisualEffect _rainVFX;

        void Start()
        {
            _rainVFX = weatherManager.Rain.rainVFX;
        }

        void Update()
        {
            // not perfect but prevents rain from entering the cabin
            _rainVFX.SetVector3("Cabin Center", transform.TransformPoint(sphereCenter));
            _rainVFX.SetFloat("Cabin Aprox Size", sphereRadius);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.DrawSphere(transform.TransformPoint(sphereCenter), sphereRadius);
        }
    }
}