﻿using System;
using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Weather.Rain
{
    public class StopRainDropsOnScreenInside : MonoBehaviour
    {
        [SerializeField] Vector3 detectionSize;
        [SerializeField] GlobalWeatherManager manager;
        [SerializeField, TagSelector] string playerTag;

        RainDropsGenerator _generator;
        Transform _transform;
        Transform _player;
        Bounds _detectionBounds;

        void Start()
        {
            _detectionBounds = new Bounds(Vector3.zero, detectionSize);
            _transform = GetComponent<Transform>();
            _player = GameObject.FindWithTag(playerTag).transform;
            _generator = manager.Rain.raindropsGenerator;
        }

        void FixedUpdate()
        {
            var relPlayerPos = _transform.InverseTransformPoint(_player.position);
            var playerIsInside = _detectionBounds.Contains(relPlayerPos);

            _generator.IsGenerating = !playerIsInside;
            AkSoundEngine.SetRTPCValue("Cabine_RTPC", playerIsInside ? 1f : 0f);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.DrawCube(transform.position, detectionSize);
        }
    }
}