﻿using UnityEngine;

namespace Spiritway.Weather.Fog
{
    [CreateAssetMenu(fileName = "FogLevel", menuName = "Spiritway/Weather/FogLevel", order = 0)]
    public class FogLevel : ScriptableObject
    {
        [Header("Smoke FX")] 
        [Range(0f, 1f)] public float smokeIntensity = 0f;
        [Range(0f, 1f)] public float cloudsIntensity = 0f;
    }
}