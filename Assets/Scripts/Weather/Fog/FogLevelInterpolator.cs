﻿using UnityEngine;

namespace Spiritway.Weather.Fog
{
    public class FogLevelInterpolator : AWeatherLevelInterpolator<FogLevel>
    {
        protected override void BeforeInterpolate()
        {
            // nothing here
        }

        protected override void Interpolate(float alpha)
        {
            WeatherLevel.smokeIntensity = Mathf.Lerp(From.smokeIntensity, To.smokeIntensity, alpha);
            WeatherLevel.cloudsIntensity = Mathf.Lerp(From.cloudsIntensity, To.cloudsIntensity, alpha);
        }
    }
}