﻿using UnityEngine;
using UnityEngine.VFX;

namespace Spiritway.Weather.Fog
{
    public class FogManager : AWeatherManager<FogLevel, FogLevelInterpolator>
    {
        static readonly int Visibility = Shader.PropertyToID("Visibility");
        
        [SerializeField] VisualEffect smokeVFX;
        [SerializeField] MeshRenderer[] cloudPlanes;

        protected override void SetupWeatherLevel()
        {
            manager.Fog = this;
        }

        protected override void UpdateWeather()
        {
            smokeVFX.SetFloat("Intensity", CurrentLevel.smokeIntensity);

            foreach (var cloudPlane in cloudPlanes)
            {
                cloudPlane.material.SetFloat(Visibility, CurrentLevel.cloudsIntensity);
            }
        }
    }
}