﻿using UnityEngine;

namespace Spiritway.Weather.Sky
{
    public class SkyManager : AWeatherManager<SkyLevel, SkyLevelInterpolator>
    {
        protected override void SetupWeatherLevel()
        {
            manager.Sky = this;
        }

        protected override void UpdateWeather()
        {
        }
    }
}