﻿using AK.Wwise;
using UnityEngine;

namespace Spiritway.Weather.Sky
{
    [CreateAssetMenu(fileName = "Sky", menuName = "Spiritway/Weather/SkyLevel", order = 0)]
    public class SkyLevel : ScriptableObject
    {
    }
}