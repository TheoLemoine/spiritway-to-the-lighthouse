﻿namespace Spiritway.Weather.Sky
{
    public class SkyLevelInterpolator : AWeatherLevelInterpolator<SkyLevel>
    {
        protected override void BeforeInterpolate()
        {
        }

        protected override void Interpolate(float alpha)
        {
            // nothing here
        }
    }
}