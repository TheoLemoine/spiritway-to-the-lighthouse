﻿using Spiritway.Weather.Fog;
using Spiritway.Weather.Ocean;
using Spiritway.Weather.Rain;
using Spiritway.Weather.Sky;
using UnityEngine;

namespace Spiritway.Weather
{
    [CreateAssetMenu(fileName = "WeatherManager", menuName = "Spiritway/Managers/Weather Manager", order = 0)]
    public class GlobalWeatherManager : ScriptableObject
    {
        public OceanManager Ocean { get; set; }
        public FogManager Fog { get; set; }
        public RainManager Rain { get; set; }
        public SkyManager Sky { get; set; }

        public void SetWeatherLevels(int ocean, int fog, int rain, int sky, float duration)
        {
            Ocean.SetLevel(ocean, duration);
            Fog.SetLevel(fog, duration);
            Rain.SetLevel(rain, duration);
            Sky.SetLevel(sky, duration);
        }
    }
}