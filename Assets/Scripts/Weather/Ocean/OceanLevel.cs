﻿using AK.Wwise;
using Crest;
using UnityEngine;

namespace Spiritway.Weather.Ocean
{
    [CreateAssetMenu(fileName = "OceanLevel", menuName = "Spiritway/Weather/OceanLevel", order = 0)]
    public class OceanLevel : ScriptableObject
    {
        [Header("Ocean shape")]
        public OceanWaveSpectrum spectrum;
    }
}