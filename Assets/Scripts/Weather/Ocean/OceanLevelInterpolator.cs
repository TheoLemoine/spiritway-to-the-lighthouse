﻿using UnityEngine;

namespace Spiritway.Weather.Ocean
{
    public class OceanLevelInterpolator : AWeatherLevelInterpolator<OceanLevel>
    {
        protected override void BeforeInterpolate()
        { }

        protected override void Interpolate(float alpha)
        {
            OceanUtility.LerpOceanWaveSpectrum(From.spectrum, To.spectrum, WeatherLevel.spectrum, alpha);
        }
    }
}