﻿using Crest;
using UnityEngine;

namespace Spiritway.Weather.Ocean
{
    public class OceanManager : AWeatherManager<OceanLevel, OceanLevelInterpolator>
    {
        [SerializeField] ShapeGerstner gerstnerWaves;

        OceanWaveSpectrum _defaultSpectrum;
        
        protected override void SetupWeatherLevel()
        {
            manager.Ocean = this;

            CurrentLevel.spectrum = ScriptableObject.CreateInstance<OceanWaveSpectrum>();
            _defaultSpectrum = gerstnerWaves._spectrum;
            gerstnerWaves._spectrum = CurrentLevel.spectrum;
        }
        
        protected override void UpdateWeather()
        {
        }

        void OnDestroy()
        {
            gerstnerWaves._spectrum = _defaultSpectrum;
        }
    }
}