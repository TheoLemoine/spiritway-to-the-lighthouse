﻿using System.Reflection;
using Crest;
using UnityEngine;

namespace Spiritway.Weather.Ocean
{
    public static class OceanUtility
    {

        public static void LerpOceanWaveSpectrum(OceanWaveSpectrum owsFrom, OceanWaveSpectrum owsTarget, OceanWaveSpectrum owsOut, float alpha)
        {
            LerpFloatField(owsFrom, owsTarget, owsOut, "_multiplier", alpha);
            LerpFloatField(owsFrom, owsTarget, owsOut, "_gravityScale", alpha);
            LerpFloatField(owsFrom, owsTarget, owsOut, "_chop", alpha);

            LerpFloatArrayField(owsFrom, owsTarget, owsOut, "_powerLog", alpha);
            LerpFloatArrayField(owsFrom, owsTarget, owsOut, "_chopScales", alpha);
            LerpFloatArrayField(owsFrom, owsTarget, owsOut, "_gravityScales", alpha);
        }

        /**
         * Lerps a float field between two objects, writes result in a third
         * (uses reflection and allows private fields)
         */
        public static void LerpFloatField<T>(T owsFrom, T owsTarget, T owsOut, string fieldName, float alpha)
        {
            var field = typeof(T).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);

            if (field == null) return;

            var from = (float)field.GetValue(owsFrom);
            var target = (float)field.GetValue(owsTarget);
            
            field.SetValue(owsOut, Mathf.Lerp(from, target, alpha));
        }

        /**
         * Lerps a array of float field between two objects, writes result in a third
         * (uses reflection and allows private fields)
         */
        public static void LerpFloatArrayField<T>(T owsFrom, T owsTarget, T owsOut, string fieldName, float alpha)
        {
            var field = typeof(T).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);

            if (field == null) return;
            
            var froms = (float[])field.GetValue(owsFrom);
            var targets = (float[])field.GetValue(owsTarget);
            
            if(froms.Length != targets.Length) return;

            var results = new float[froms.Length];

            for (int i = 0; i < froms.Length; i++)
            {
                results[i] = Mathf.Lerp(froms[i], targets[i], alpha);
            }
            
            field.SetValue(owsOut, results);
        }
    }
}