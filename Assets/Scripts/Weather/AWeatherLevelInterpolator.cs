﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Weather
{
    public abstract class AWeatherLevelInterpolator<T> where T : ScriptableObject
    {
        public T WeatherLevel { get; set; }
        protected T From { get; set; }
        protected T To { get; set; }
        
        float _duration;

        public void Init(T from, T to, T used, float duration)
        {
            _duration = duration;
            From = from;
            To = to;
            WeatherLevel = used;
        }
        
        /**
         * Coroutine you can run on another game object to do the interpolation
         */
        public IEnumerator RunInterpolation(Action interpolateCallback)
        {
            BeforeInterpolate();

            for (float elapsed = 0; elapsed < _duration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / _duration;

                Interpolate(progress);
                interpolateCallback();
                
                yield return null;
            }
            
            Interpolate(1);
            interpolateCallback();
        }
        
        /**
         * Called before interpolation, to set non interpolable properties
         */
        protected abstract void BeforeInterpolate();
        
        /**
         * called each step of the interpolation, to smoothly transition properties
         */
        protected abstract void Interpolate(float alpha);
    }
}