﻿using System;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Maelstrom
{
    public class MaelstromForce : MonoBehaviour
    {
        [SerializeField, TagSelector] string boatTag;
        [SerializeField] float radius;
        [SerializeField] float flowStrength;
        [SerializeField] float pullStrength;

        GameObject _boat;
        Rigidbody _boatRb;
        Transform _transform;

        void Start()
        {
            _boat = GameObject.FindWithTag(boatTag);
            _boatRb = _boat.GetComponent<Rigidbody>();
            
            _transform = GetComponent<Transform>();
        }

        void FixedUpdate()
        {
            var toBoat = _boatRb.position - _transform.position;

            var force = MathHelpers.Remap(toBoat.magnitude, radius, 0, 0, 1);
            if (force <= 0) return;

            var toBoatFlatNorm = new Vector3(toBoat.x, 0, toBoat.z).normalized;
            var flowDirection = Vector3.Cross(toBoatFlatNorm, Vector3.down);
            
            var totalForce = Vector3.zero;
            totalForce += flowDirection * (flowStrength * force);
            totalForce += -toBoat.normalized * (pullStrength * force);
            
            _boatRb.AddForce(totalForce, ForceMode.Acceleration);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}