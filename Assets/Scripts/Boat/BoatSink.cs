﻿using System;
using System.Collections;
using System.Reflection;
using Crest;
using UnityEngine;

namespace Spiritway.Boat
{
    [RequireComponent(typeof(BoatHull), typeof(BoatProbes))]
    public class BoatSink : MonoBehaviour
    {
        [SerializeField] float duration = 4f;
        [SerializeField] RegisterClipSurfaceInput waterClip;
        [SerializeField] AK.Wwise.Event sinkSoundStart;
        [SerializeField] AK.Wwise.Event sinkSoundStop;
        
        BoatHull _hull;
        BoatProbes _probes;

        FieldInfo _forceMultField;
        float _baseForceMultValue;
        
        void Start()
        {
            _probes = GetComponent<BoatProbes>();
            
            _hull = GetComponent<BoatHull>();
            _hull.OnTimeoutEnded += SinkBoat;
            
            _forceMultField = typeof(BoatProbes).GetField("_forceMultiplier", BindingFlags.NonPublic | BindingFlags.Instance);

            if (_forceMultField == null)
            {
                Debug.LogError("Could not get private buoyancy field");
                return;
            }

            _baseForceMultValue = (float)_forceMultField.GetValue(_probes);
        }

        void SinkBoat()
        {
            waterClip.enabled = false;
            StartCoroutine(LerpBuoyancy());
            sinkSoundStart.Post(gameObject);
        }

        /**
         * Resets the buoyancy values of the boat to normal.
         */
        public void UnSinkBoat()
        {
            _forceMultField.SetValue(_probes, _baseForceMultValue);
            sinkSoundStop.Post(gameObject);
        }

        IEnumerator LerpBuoyancy()
        {
            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / duration;
                var nextValue = Mathf.Lerp(_baseForceMultValue, 0, progress);
                _forceMultField.SetValue(_probes, nextValue);
                yield return null;
            }
        }
    }
}