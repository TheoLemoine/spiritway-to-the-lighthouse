﻿using Spiritway.Player;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Spiritway.Boat
{
    /**
     * The helm is where the player will control boat speed and direction
     */
    public class BoatHelm : ABoatLockedInteractable
    {
        [SerializeField] Transform gearTransform;
        [SerializeField] Transform rudderTransform;
        [SerializeField] BoatEngine engine;
        [SerializeField] BoatHull hull;

        [Header("Rudder")]
        // how much the rudder angle changes
        [SerializeField] float rudderRotationMultiplier = 100;
        // how much the engine TorquePerSpeed value changes
        [SerializeField] float engineTorqueMultiplier = 0.1f;

        [SerializeField] float minRotation = -100;
        [SerializeField] float maxRotation = 100;

        [Header("Rudder Sounds")] 
        [SerializeField] AK.Wwise.Event rudderStartSound;
        [SerializeField] AK.Wwise.Event rudderStopSound;
        [SerializeField] AK.Wwise.Event rudderCenterSound;

        [Header("Gear")] 
        [SerializeField] float rotationPerGear = 20f;
        [SerializeField] [Range(0.01f, 0.2f)] float gearChangeSmoothness = 0.1f;
        [SerializeField] int minGear = -2;
        [SerializeField] int maxGear = 2;

        [Header("Haptic feedbacks")] 
        [SerializeField] HapticsManager haptics;
        
        PlayerActions _actions;

        bool _isRudderSoundPlaying = false;

        float _rudderRotationSpeed = 0;
        float _rudderCurrentRotation = 0;
        HapticsManager.ContinuousHaptic _rudderHaptic;

        VisualElement _rtTip;
        VisualElement _ltTip;
        VisualElement _engineTip;
        bool _rtTipShow = true;
        bool _ltTipShow = true;
        bool _hasHit = false;

        void Start()
        {
            _actions = new PlayerActions();
            
            _actions.BoatControls.Exit.performed += OnExit;
            
            _actions.BoatControls.Rudder.performed += OnRudder;
            _actions.BoatControls.Rudder.canceled += OnRudder;
            
            _actions.BoatControls.Gearshift_Up.performed += OnGearshiftUp;
            _actions.BoatControls.Gearshift_Down.performed += OnGearshiftDown;

            _rudderHaptic = haptics.MakeContinuousHaptic(0, 0.02f);

            hull.OnBoatHit += FirstHitReactivateUI;
        }

        void FixedUpdate()
        {
            var rudderPrevRotation = _rudderCurrentRotation;
            
            _rudderCurrentRotation += _rudderRotationSpeed;
            _rudderCurrentRotation = Mathf.Clamp(_rudderCurrentRotation, minRotation, maxRotation);
            engine.TorquePerSpeed = _rudderCurrentRotation * engineTorqueMultiplier;

            // rudder sounds
            var absRudderSpeed = Mathf.Abs(_rudderCurrentRotation - rudderPrevRotation);
            AkSoundEngine.SetRTPCValue("Barre_Speed_RTPC", absRudderSpeed);
            var rudderIsMoving = absRudderSpeed > 0.01f;
            
            // update move sound
            if (rudderIsMoving && !_isRudderSoundPlaying)
            {
                rudderStartSound.Post(gameObject);
                _rudderHaptic.Activate();
                _isRudderSoundPlaying = true;
            }
            else if (!rudderIsMoving && _isRudderSoundPlaying)
            {
                rudderStopSound.Post(gameObject);
                _rudderHaptic.Deactivate();
                _isRudderSoundPlaying = false;
            }

            // passed by cebter
            if (rudderPrevRotation < 0 && _rudderCurrentRotation > 0 
                || rudderPrevRotation > 0 && _rudderCurrentRotation < 0)
            {
                rudderCenterSound.Post(gameObject);
            }
        }

        void Update()
        {
            rudderTransform.localRotation = Quaternion.Euler(0, 0, _rudderCurrentRotation * rudderRotationMultiplier);
            
            var targetRot = Quaternion.Euler(0, 0, engine.Gear * rotationPerGear);
            gearTransform.localRotation = Quaternion.Lerp(gearTransform.localRotation, targetRot, gearChangeSmoothness);
        }

        public override string GetInteractionName() => "Sail";
        
        protected override void OnHUDVisible()
        {
            _rtTip = HudScreen.Q("right_trigger");
            _ltTip = HudScreen.Q("left_trigger");
            _engineTip = HudScreen.Q("engine_tip");

            if (!_rtTipShow) _rtTip.style.opacity = 0;
            if (!_ltTipShow) _ltTip.style.opacity = 0;
            _engineTip.style.opacity = 0;
        }

        protected override void OnControlGained()
        {
            _actions.Enable();
        }

        protected override void OnControlLost()
        {
            _actions.Disable();
        }

        void FirstHitReactivateUI()
        {
            if (!_hasHit)
            {
                StartCoroutine(UIHelper.SmoothLerpOpacity(_ltTip, 0, 1, 1));
                _ltTipShow = true;
                
                _hasHit = true;
            }
        }
        
        // -- Input callbacks
        
        void OnExit(InputAction.CallbackContext context)
        {
            StopInteraction();
        }
        
        void OnRudder(InputAction.CallbackContext context)
        {
            _rudderRotationSpeed = context.ReadValue<float>();
        }
        
        void OnGearshiftDown(InputAction.CallbackContext obj)
        {
            engine.Gear = Mathf.Clamp(engine.Gear - 1, minGear, maxGear);

            if (_ltTipShow)
            {
                StartCoroutine(UIHelper.SmoothLerpOpacity(_ltTip, 1, 0, 1));
                _ltTipShow = false;

                if (_hasHit && hull.Lives < hull.MaxLives)
                {
                    StartCoroutine(TimeHelper.Delay(1f, UIHelper.SmoothLerpOpacity(_engineTip, 0, 1, 1)));
                }
            }
        }

        void OnGearshiftUp(InputAction.CallbackContext obj)
        {
            engine.Gear = Mathf.Clamp(engine.Gear + 1, minGear, maxGear);

            if (_rtTipShow && engine.Gear > 1)
            {
                StartCoroutine(UIHelper.SmoothLerpOpacity(_rtTip, 1, 0, 1));
                _rtTipShow = false;
            }
        }
    }
}