using System.Collections;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Boat
{
    public class BoatLightFlicker : MonoBehaviour
    {
        [SerializeField] AK.Wwise.Event flickerSound;
        [SerializeField] float minWait = 10f;
        [SerializeField] float maxWait = 15f;
        [SerializeField] float timeDown = 0.2f;
        [SerializeField] float timeUpping = 0.2f;
        [SerializeField] float doubleFlickerProbability = 0.4f;
        
        Light _light;
        float _baseIntensity;
        
        void Start()
        {
            _light = GetComponent<Light>();
            _baseIntensity = _light.intensity;
            
            StartCoroutine(FlickerWait());
        }

        IEnumerator FlickerWait()
        {
            for (;;)
            {
                var waitTime = Random.Range(minWait, maxWait);
                yield return new WaitForSeconds(waitTime);
                
                yield return Flicker();
                
                if (Random.Range(0f, 1f) > doubleFlickerProbability)
                {
                    yield return Flicker();
                }
            }
        }

        IEnumerator Flicker()
        {
            _light.intensity = 0;
            flickerSound.Post(gameObject);
            yield return new WaitForSeconds(timeDown);
            yield return LightHelper.LerpLightIntensity(_light, _baseIntensity, timeUpping);
        }
    }
}
