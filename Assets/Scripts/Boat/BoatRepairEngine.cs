﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spiritway.Player;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.Boat
{
    public class BoatRepairEngine : ABoatLockedInteractable
    {
        [SerializeField] BoatHull hull;
        [SerializeField] float repairOneStepDuration;

        [SerializeField] List<AK.Wwise.Event> voiceLines;
        [SerializeField] List<AK.Wwise.Event> repairSounds;
        [SerializeField] AK.Wwise.Event stopRepairSounds;

        VisualElement _progressBar;
        PlayerActions _actions;

        bool _linePlaying;

        void Start()
        {
            _actions = new PlayerActions();
        }

        protected override void OnHUDVisible()
        {
            _progressBar = HudScreen.Q("repair_bar");
            _progressBar.style.width = Length.Percent(0);
            _progressBar.style.height = Length.Percent(0);
        }

        public override bool CanInteract() 
            => base.CanInteract() && hull.Lives < hull.MaxLives && !hull.IsDead();
        public override string GetInteractionName() => "Repair";

        protected override void OnControlGained()
        {
            _actions.Enable();
            StartCoroutine(RepairEngine());

            if (!_linePlaying)
            {
                voiceLines[hull.Lives].Post(gameObject);
                _linePlaying = true;
                StartCoroutine(TimeHelper.Delay(5f, () => _linePlaying = false));
            }

            repairSounds[hull.Lives].Post(gameObject);
        }

        protected override void OnControlLost()
        {
            _actions.Disable();
            stopRepairSounds.Post(gameObject);
        }

        IEnumerator RepairEngine()
        {
            var nbRepairs = hull.MaxLives - hull.Lives;
            var totalRepairDuration = repairOneStepDuration * nbRepairs;
            var nextStepRepair = repairOneStepDuration;
            
            for (float elapsed = 0; elapsed < totalRepairDuration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / totalRepairDuration;

                if (elapsed >= nextStepRepair)
                {
                    hull.Repair();
                    nextStepRepair += repairOneStepDuration;
                }
                
                // update loading bar, make sounds, etc
                _progressBar.style.width = Length.Percent(progress * 100);
                _progressBar.style.height = Length.Percent(progress * 100);

                yield return null;
                
                // if were not maintaining
                if (_actions.SailorControls.Interact.ReadValue<float>() <= 0f)
                {
                    StopInteraction();
                    yield break;
                }
            }
            
            hull.Repair();
            StopInteraction();
        }
    }
}