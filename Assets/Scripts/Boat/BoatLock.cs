﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Boat
{
    /**
     * Allows locking object with Rigidbody relative to the boat position
     */
    public class BoatLock : MonoBehaviour
    {
        [SerializeField] public float moveToDuration = 1f;
        
        Transform _transform;
        Rigidbody _rb;
        
        void Start()
        {
            _transform = GetComponent<Transform>();
            _rb = GetComponent<Rigidbody>();
        }

        public void Lock(Rigidbody rb, Vector3 absPos, Action onLockedCallback)
        {
            var relPos = _transform.InverseTransformPoint(absPos);

            rb.detectCollisions = false;
            rb.useGravity = false;
            
            StartCoroutine(MoveToAndLock(rb, relPos, onLockedCallback));
        }

        public void Unlock(Rigidbody rb)
        {
            // remove lock component if there is one
            if (rb.gameObject.TryGetComponent(out ConfigurableJoint joint))
            {
                rb.detectCollisions = true;
                rb.useGravity = true;
                
                Destroy(joint);
            }
            else
            {
                Debug.LogWarning("Trying to unlock rb before it was locked to boat");
            }
        }

        public void Move(Rigidbody rb, Vector3 newAbsPos)
        {
            if (rb.gameObject.TryGetComponent(out ConfigurableJoint joint))
            {
                joint.connectedBody = null;
                
                rb.transform.position = newAbsPos;
                
                joint.connectedBody = _rb;
            }
            else
            {
                Debug.LogWarning("Trying to move not locked component");
                rb.position = newAbsPos;
            }
            
        }

        IEnumerator MoveToAndLock(Rigidbody rb, Vector3 targetRelPos, Action onLockedCallback)
        {
            // smoothly move the object to desired position
            var startRelPos = _transform.InverseTransformPoint(rb.position);
            
            for (var elapsed = 0f; elapsed < moveToDuration; elapsed += Time.fixedDeltaTime)
            {
                var progress = elapsed / moveToDuration;
                var nextRelPos = Vector3.Lerp(startRelPos, targetRelPos, progress);
                var nextAbsPos = _transform.TransformPoint(nextRelPos);
                
                rb.MovePosition(nextAbsPos);
                
                yield return new WaitForFixedUpdate();
            }

            // and add lock component, now, rb is truly locked
            var joint = rb.gameObject.AddComponent<ConfigurableJoint>();
            joint.connectedBody = _rb;
            // lock position but not rotation
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;
            
            onLockedCallback.Invoke();
        }
        
    }
}