﻿using System;
using System.Collections;
using Spiritway.Player;
using Spiritway.Sailor;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Boat
{
    /**
     * Handle collision with rocks or other objects, and remembers damage taken
     */
    [RequireComponent(typeof(Rigidbody))]
    public class BoatHull : MonoBehaviour
    {
        public event Action OnBoatHit;
        public event Action OnBoatSmallHit;
        public event Action OnBoatRepaired;
        public event Action OnTimeoutStarted;
        public event Action OnTimeoutStopped;
        public event Action OnTimeoutEnded;
        
        [SerializeField] int maxLives = 3;
        [SerializeField] LayerMask collideWith;
        [SerializeField] float impulseForSmallHit = 500;
        [SerializeField] float impulseForHitDamage = 10000;

        [SerializeField] float secondsBeforeGameOver;
        [SerializeField] float secondsInvulnerableAfterHit;

        [SerializeField] HapticsManager haptics;
        [SerializeField] SailorScreenShake shake;

        [Header("Sound")]
        [SerializeField] AK.Wwise.Event crashSound;
        [SerializeField] float timeBeforeSwear = 0.5f;
        [SerializeField] AK.Wwise.Event sailorSwearSound;

        public int Lives { get; private set; }
        public int MaxLives => maxLives;
        Coroutine _gameOverCoroutine;
        bool _isInvulnerable;
        bool _isDead;
        Coroutine _invulnerableCoroutine;
        
        void Start()
        {
            Lives = maxLives;
        }

        void OnCollisionEnter(Collision other)
        {
            if (_isInvulnerable) return;

            // skip if boat speed is not enough to make damage
            if (other.impulse.sqrMagnitude < impulseForHitDamage * impulseForHitDamage)
            {
                if (other.impulse.sqrMagnitude > impulseForSmallHit * impulseForSmallHit)
                {
                    OnBoatSmallHit?.Invoke(); // no damage hit (small poc sounds, etc)
                }
                
                return;
            }

            if (((1 << other.gameObject.layer) & collideWith) != 0) // layer check
            {
                Lives--;
                OnBoatHit?.Invoke();
                
                crashSound.Post(other.gameObject);

                StartCoroutine(TimeHelper.Delay(timeBeforeSwear, () => sailorSwearSound.Post(gameObject)));
                
                // feedbacks
                StartCoroutine(haptics.AddTemporaryHaptic(0.6f, 0.7f, 0.8f));
                shake.Shake(1f);

                // stop coroutine if already running
                if (_invulnerableCoroutine != null)
                {
                    StopCoroutine(_invulnerableCoroutine);
                    _invulnerableCoroutine = null;
                }
                
                _invulnerableCoroutine = StartCoroutine(MakeInvulnerableForXSeconds(secondsInvulnerableAfterHit));
            }

            // start timeout before game over
            if (Lives <= 0 && _gameOverCoroutine == null)
            {
                _gameOverCoroutine = StartCoroutine(GameOverAfterXSeconds(secondsBeforeGameOver));

                OnTimeoutStarted?.Invoke();
            }
        }

        public void Repair()
        {
            Lives = Mathf.Min(Lives + 1, maxLives);
            
            OnBoatRepaired?.Invoke();
            
            // Stop timeout if running
            if (_gameOverCoroutine != null)
            {
                StopCoroutine(_gameOverCoroutine);
                _gameOverCoroutine = null;
                OnTimeoutStopped?.Invoke();
            }
        }

        public void ResetHull()
        {
            Lives = maxLives;
            _isDead = false;
            OnBoatRepaired?.Invoke();
            OnTimeoutStopped?.Invoke();
            
            if (_gameOverCoroutine != null)
            {
                StopCoroutine(_gameOverCoroutine);
                _gameOverCoroutine = null;
            }
        }

        public bool IsDead()
        {
            return _isDead;
        }

        IEnumerator GameOverAfterXSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            
            OnTimeoutEnded?.Invoke();
            _isDead = true;
        }

        IEnumerator MakeInvulnerableForXSeconds(float seconds)
        {
            _isInvulnerable = true;
            yield return new WaitForSeconds(seconds);
            _isInvulnerable = false;
        }
    }
}