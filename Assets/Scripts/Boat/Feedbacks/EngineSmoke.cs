﻿using System;
using UnityEngine;
using UnityEngine.VFX;

namespace Spiritway.Boat.Feedbacks
{
    [RequireComponent(typeof(VisualEffect))]
    public class EngineSmoke : MonoBehaviour
    {
        [SerializeField] BoatHull hull;
        [SerializeField] float[] effectIntensities;
        
        VisualEffect _smokeEffect;

        void Start()
        {
            _smokeEffect = GetComponent<VisualEffect>();
            
            hull.OnBoatHit += UpdateSmoke;
            hull.OnBoatRepaired += UpdateSmoke;
        }

        void UpdateSmoke()
        {
            _smokeEffect.SetFloat("Intensity", effectIntensities[hull.MaxLives - hull.Lives]);
        }
    }
}