﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Boat.Feedbacks
{
    public class EngineLights : MonoBehaviour
    {
        [SerializeField] BoatHull hull;
        [SerializeField] MeshRenderer[] lights;

        [SerializeField] Material matUp;
        [SerializeField] Material matDown;

        IEnumerator Start()
        {
            hull.OnBoatHit += UpdateLights;
            hull.OnBoatRepaired += UpdateLights;

            yield return null;
            
            UpdateLights();
        }

        void UpdateLights()
        {
            for (var i = 0; i < lights.Length; i++)
            {
                lights[i].material = i < hull.Lives ? matUp : matDown;
            }
        }
    }
}