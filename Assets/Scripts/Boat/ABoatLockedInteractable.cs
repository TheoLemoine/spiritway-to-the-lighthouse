﻿using System;
using System.Collections;
using System.Linq;
using Spiritway.Sailor;
using Spiritway.UI;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.Boat
{
    /**
     * Abstract class to derive from when implementing interactables that
     * take controls of the player and lock it at a position
     */
    [RequireComponent(typeof(Collider))]
    public abstract class ABoatLockedInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] BoatLock boatLock;
        [SerializeField] Transform lockAtPos;
        [SerializeField] VisualTreeAsset uiScreen;
        [SerializeField] UIManager uiManager;
        [SerializeField] Vector2 centerCamRotation;
        
        protected IInteractor Interactor;
        protected GameObject InteractorGO;
        protected Rigidbody InteractorRb;
        protected VisualElement HudScreen;

        bool _isActive = true;
        bool _isInteracting = false;

        void Awake()
        {
            HudScreen = uiScreen.Instantiate().Children().First();
        }

        public virtual bool CanInteract() => _isActive;
        public virtual string GetInteractionName() => "Interact";

        public void DeactivateInteraction()
        {
            if(_isInteracting)
                StopInteraction();
            
            _isActive = false;
        }

        public void StartInteraction(IInteractor interactor, GameObject obj)
        {
            if(!_isActive) return;
            
            OnHUDVisible();
            uiManager.AddScreenToHud(HudScreen);
            Interactor = interactor;
            InteractorGO = obj;

            if (obj.TryGetComponent(out InteractorRb))
            {
                boatLock.Lock(InteractorRb, lockAtPos.position, OnControlGained);
            }
            else
            {
                Debug.LogError("Interactor must have a rigidbody to interact with this component");
            }

            if (obj.TryGetComponent(out SailorController controller))
            {
                StartCoroutine(controller.LerpCamRotationTo(centerCamRotation, boatLock.moveToDuration));
            }

            _isInteracting = true;
        }
        
        protected void StopInteraction()
        {
            OnControlLost();
            
            HudScreen.RemoveFromHierarchy();
            boatLock.Unlock(InteractorRb);
            Interactor?.StopCurrentInteraction();
            Interactor = null;

            _isInteracting = false;
        }

        protected void OffsetLockedPosition(Vector3 offset)
        {
            boatLock.Move(InteractorRb, lockAtPos.position + offset);
        }

        /**
         * Called just before the HUD becomes visible, to setup things
         */
        protected abstract void OnHUDVisible();
        
        /**
         * Called when the player is locked in place, available for interaction
         */
        protected abstract void OnControlGained();

        /**
         * Called just before the player regains his freedom of movement
         */
        protected abstract void OnControlLost();


    }
}