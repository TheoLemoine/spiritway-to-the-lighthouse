using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Boat
{
    /**
     * Allows to deactivate all lights but the one of the projector
     * And reactivate them later
     */
    public class BoatLights : MonoBehaviour
    {
        [SerializeField] Light projectorLight;
        
        Light[] _boatLights;
        float[] _baseIntensities;

        void Start()
        {
            _boatLights = GetComponentsInChildren<Light>();
            _baseIntensities = new float[_boatLights.Length];
        }
        
        public void SetAllLights(bool active)
        {
            for (int i = 0; i < _boatLights.Length; i++)
            {
                if (_boatLights[i] == projectorLight) continue;
                
                StartCoroutine(
                    LightHelper.LerpLightIntensity(_boatLights[i], active ? _baseIntensities[i] : 0, 0)
                );
            }
        }
    }
}
