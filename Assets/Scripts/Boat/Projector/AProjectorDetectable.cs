﻿using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Boat.Projector
{
    public abstract class AProjectorDetectable : MonoBehaviour
    {
        [SerializeField, TagSelector] string boatTag;
        
        ProjectorDetector _detector;
        bool _isVisible;
        
        protected virtual void Start()
        {
            _detector = GameObject.FindWithTag(boatTag).GetComponentInChildren<ProjectorDetector>();
        }

        void Update()
        {
            var isNowVisible = _detector.CanSee(transform.position);
            
            if (!_isVisible && isNowVisible) OnProjectorVisibilityGained();
            else if (_isVisible && !isNowVisible) OnProjectorVisibilityLost();
            else if (_isVisible && isNowVisible) OnProjectorVisibilityStay();

            _isVisible = isNowVisible;
        }

        /**
         * Called when object becomes visible by projector
         */
        protected virtual void OnProjectorVisibilityGained() {}
        
        /**
         * Called when object is no more visible by projector
         */
        protected virtual void OnProjectorVisibilityLost() {}
        
        /**
         * Called every frame the object is still visible
         */
        protected virtual void OnProjectorVisibilityStay() {}
    }
}