﻿using Spiritway.Player;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Spiritway.Boat.Projector
{
    public class BoatProjector : ABoatLockedInteractable
    {
        [SerializeField] Transform horizontalRotator;
        [SerializeField] Transform verticalRotator;

        [SerializeField] Vector2 rotationSpeed;
        [SerializeField] Vector2 limits;

        [Header("Tutorial")] 
        [SerializeField] GlobalEvent tutorialEvent;
        
        [Header("Sound")] 
        [SerializeField] AK.Wwise.Event playRotationSound;
        [SerializeField] AK.Wwise.Event stopRotationSound;

        [Header("Camera")] 
        [SerializeField] float offsetSpeed;
        [SerializeField] float positionOffset;
        
        PlayerActions _actions;
        Transform _cameraTransform;
        
        Vector2 _projAngSpeed = Vector2.zero;
        Vector2 _projRot = Vector2.zero;

        bool _rotationSoundPlaying;
        
        float _targetOffset;
        float _offset;
        
        bool _rtTipShow = true;
        bool _ltTipShow = true;
        bool _spiritTipShow = true;
        VisualElement _rtTip;
        VisualElement _ltTip;
        VisualElement _spiritTip;
        
        void Start()
        {
            _actions = new PlayerActions();

            _actions.ProjectorControls.Rotate.performed += OnRotate;
            _actions.ProjectorControls.Rotate.canceled += OnRotate;
            
            _actions.ProjectorControls.Exit.performed += OnExit;
            
            _actions.ProjectorControls.Look_Left.performed += OnLookLeft;
            _actions.ProjectorControls.Look_Left.canceled += OnLookLeft;

            _actions.ProjectorControls.Look_Right.performed += OnLookRight;
            _actions.ProjectorControls.Look_Right.canceled += OnLookRight;
        }

        void FixedUpdate()
        {
            var prevProjRot = _projRot;
            
            _projRot += _projAngSpeed * Time.fixedDeltaTime;
            
            // clamp projector rotation to limit
            _projRot.x = Mathf.Clamp(_projRot.x, -limits.x, limits.x);
            _projRot.y = Mathf.Clamp(_projRot.y, -limits.y, limits.y);

            var rotDiff = _projRot - prevProjRot;
            var isRotating = rotDiff.sqrMagnitude > 0.01f;

            if (!_rotationSoundPlaying && isRotating)
            {
                playRotationSound.Post(gameObject);
                _rotationSoundPlaying = true;
            }
            else if(_rotationSoundPlaying && !isRotating)
            {
                stopRotationSound.Post(gameObject);
                _rotationSoundPlaying = false;
            }
            
            // update offset
            var offsetDiff = _targetOffset - _offset;
            if (Mathf.Abs(offsetDiff) > 0.05f)
            {
                _offset += offsetSpeed * Time.fixedDeltaTime * Mathf.Sign(offsetDiff);
                var projRelOffset = transform.TransformDirection(Vector3.right * _offset + Vector3.down * Mathf.Abs(_offset * 0.1f));
                OffsetLockedPosition(projRelOffset);
                _cameraTransform.localRotation = Quaternion.Euler(0, 0, _offset * -4);
            }
        }

        void Update()
        {
            horizontalRotator.localRotation = Quaternion.Euler(0, _projRot.x, 0);
            verticalRotator.localRotation = Quaternion.Euler(-_projRot.y, 0, 0);
        }
        
        public override string GetInteractionName() => "Reveal Spirits";

        protected override void OnHUDVisible()
        { 
            _rtTip = HudScreen.Q("right_trigger");
            _ltTip = HudScreen.Q("left_trigger");
            _spiritTip = HudScreen.Q("spirit_tip");

            if (!_rtTipShow) _rtTip.style.opacity = 0;
            if (!_ltTipShow) _ltTip.style.opacity = 0;
            if (!_spiritTipShow) _spiritTip.style.opacity = 0;
        }

        protected override void OnControlGained()
        {
            _actions.Enable();
            tutorialEvent.Call();
            _cameraTransform = InteractorGO.GetComponentInChildren<Camera>().transform.parent;
        }

        protected override void OnControlLost()
        {
            _actions.Disable();
            
            _cameraTransform.localRotation = Quaternion.identity;
        }

        // -- input callbacks
        
        void OnExit(InputAction.CallbackContext context)
        {
            StopInteraction();
        }
        
        void OnRotate(InputAction.CallbackContext context)
        {
            _projAngSpeed = context.ReadValue<Vector2>() * rotationSpeed;

            if (_spiritTipShow)
            {
                _spiritTipShow = false;
                StartCoroutine(TimeHelper.Delay(6f, 
                    () => StartCoroutine(UIHelper.SmoothLerpOpacity(_spiritTip, 1, 0, 1))
                ));
            }
            
        }

        void OnLookLeft(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _targetOffset -= positionOffset;
            }
            else if(context.canceled)
            {
                _targetOffset += positionOffset;
            }
            
            if (_ltTipShow)
            {
                StartCoroutine(UIHelper.SmoothLerpOpacity(_ltTip, 1, 0, 1));
                _ltTipShow = false;
            }
        }
        
        void OnLookRight(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _targetOffset += positionOffset;
            }
            else if(context.canceled)
            {
                _targetOffset -= positionOffset;
            }
            
            if (_rtTipShow)
            {
                StartCoroutine(UIHelper.SmoothLerpOpacity(_rtTip, 1, 0, 1));
                _rtTipShow = false;
            }
        }

    }
}