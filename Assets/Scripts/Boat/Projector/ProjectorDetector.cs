﻿using System;
using UnityEngine;

namespace Spiritway.Boat.Projector
{
    public class ProjectorDetector : MonoBehaviour
    {
        [SerializeField] Light projectorLight;
        [SerializeField] Transform projectorTransform;
        [SerializeField, Range(0f, 1f)] float rangeMultiplier = 0.25f;
        [SerializeField] LayerMask raycastMask;

        float _projAngle;
        float _projRange;


        void Start()
        {
            if(projectorLight.type != LightType.Spot) Debug.LogWarning("Projector light should be a spotlight");
            
            projectorTransform = projectorLight.transform;
            _projAngle = (projectorLight.spotAngle - projectorLight.innerSpotAngle) / 2;
            _projRange = projectorLight.range * rangeMultiplier;
        }

        /**
         * Checks if given object is in projectors light cone
         */
        public bool CanSee(Vector3 pos)
        {
            var toPos = pos - projectorTransform.position;
            var distToPos = toPos.magnitude;
            
            // check distance first
            if (distToPos > _projRange) 
                return false;

            // then angle
            var angle = Vector3.Angle(projectorTransform.forward, toPos);

            if (angle > _projAngle) return false;

            // then check obstacles
            return !Physics.Raycast(projectorTransform.position, toPos, distToPos, raycastMask);
        }
        
    }
}