﻿using Crest;
using Spiritway.Respawn;
using UnityEngine;

namespace Spiritway.Boat
{
    [RequireComponent(typeof(BoatEngine), typeof(BoatSink), typeof(BoatHull))]
    public class BoatRespawn : ARespawnableBehaviour
    {
        [SerializeField] RegisterClipSurfaceInput waterClip;
        Transform _transform;
        BoatSink _sinker;
        BoatEngine _engine;
        BoatHull _hull;
        
        Vector3 _savedPosition;
        Quaternion _savedRotation;

        protected override void Start()
        {
            _transform = GetComponent<Transform>();
            _engine = GetComponent<BoatEngine>();
            _sinker = GetComponent<BoatSink>();
            _hull = GetComponent<BoatHull>();
            
            base.Start();
        }

        public override void Save()
        {
            _savedPosition = _transform.position;
            _savedRotation = _transform.rotation;
        }

        public override void Respawn()
        {
            _sinker.UnSinkBoat();
            _engine.Gear = 0;
            _hull.ResetHull();

            _transform.position = _savedPosition;
            _transform.rotation = _savedRotation;
            waterClip.enabled = true;
        }
    }
}