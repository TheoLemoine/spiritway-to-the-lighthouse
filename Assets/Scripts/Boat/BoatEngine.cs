﻿using System.Collections.Generic;
using AK.Wwise;
using UnityEngine;

namespace Spiritway.Boat
{
    /**
     * Behaviour responsible for propelling the boat
     */
    [RequireComponent(typeof(Rigidbody))]
    public class BoatEngine : MonoBehaviour
    {
        // Gear that multiply default engine force
        int _gear = 0;
        public int Gear
        {
            get => _gear;
            set
            {
                engineSwitches[Mathf.Abs(value)].SetValue(engineSoundObject);
                _gear = value;
            }
        }

        // force applied in torque when the boat move 1m
        public float TorquePerSpeed { get; set; } = -0.12f;
        
        [SerializeField] int defaultGear = 1;
        [SerializeField] float engineForce = 2.5f;
        [SerializeField] BoatHull hull;
        
        [Header("Sounds")]
        [SerializeField] GameObject engineSoundObject;
        [SerializeField] List<Switch> engineSwitches;
        [SerializeField] AK.Wwise.Event engineCrashSound;
        [SerializeField] AK.Wwise.Event engineHardCrashSound;
        [SerializeField] AK.Wwise.Event stopHardCrashSound;
        [SerializeField] AK.Wwise.Event alarmSoundStart;
        [SerializeField] AK.Wwise.Event alarmSoundStop;
        
        Rigidbody _rb;
        Transform _transform;

        void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _transform = GetComponent<Transform>();

            Gear = defaultGear;
            
            hull.OnBoatHit += () =>
            {
                Gear = 0;

                if (hull.Lives <= 1)
                {
                    engineHardCrashSound.Post(engineSoundObject);
                }
                else
                {
                    engineCrashSound.Post(engineSoundObject);
                }
            };
            
            hull.OnBoatRepaired += () =>
            {
                if (hull.Lives > 1)
                {
                    stopHardCrashSound.Post(engineSoundObject);
                }
            };

            hull.OnTimeoutStarted += () => alarmSoundStart.Post(engineSoundObject);
            hull.OnTimeoutEnded += () => alarmSoundStop.Post(engineSoundObject);
            hull.OnTimeoutStopped += () => alarmSoundStop.Post(engineSoundObject);
        }

        void FixedUpdate()
        {
            _rb.AddRelativeForce(0, 0, Gear * engineForce, ForceMode.Acceleration);

            var forwardVel = _transform.InverseTransformDirection(_rb.velocity).z;
            _rb.AddRelativeTorque(0, forwardVel * TorquePerSpeed, 0, ForceMode.Acceleration);
        }
    }
}