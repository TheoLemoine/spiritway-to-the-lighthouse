﻿namespace Spiritway
{
    public interface IInteractor
    {
        /**
         * Called by interactables when they request to end the interaction
         */
        void StopCurrentInteraction();
    }
}