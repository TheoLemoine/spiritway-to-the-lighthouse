﻿using System.Collections.Generic;
using UnityEngine;

namespace Spiritway.Respawn
{
    /**
     * This manager handles checkpoint save and respawn logic.
     */
    [CreateAssetMenu(fileName = "RespawnManager", menuName = "Spiritway/Managers/Respawn Manager", order = 0)]
    public class RespawnManager : ScriptableObject
    {
        List<IRespawnable> Respawnables { get; set; }

        void OnEnable()
        {
            Respawnables = new List<IRespawnable>();
        }

        public void Register(IRespawnable respawnable)
        {
            Respawnables.Add(respawnable);
        }

        public void Unregister(IRespawnable respawnable)
        {
            Respawnables.Remove(respawnable);
        }

        public void TriggerRespawn()
        {
            foreach (var respawnable in Respawnables)
            {
                respawnable.Respawn();
            }
        }

        public void TriggerSave()
        {
            foreach (var respawnable in Respawnables)
            {
                respawnable.Save();
            }
        }

    }
}