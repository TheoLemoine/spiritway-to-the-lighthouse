﻿using System;
using UnityEngine;

namespace Spiritway.Respawn
{
    /**
     * Defines a monobehaviour responsible for saving and setting data of other components on respawn
     */
    public abstract class ARespawnableBehaviour : MonoBehaviour, IRespawnable
    {
        [SerializeField] protected RespawnManager respawnManager;
        
        protected virtual void Start()
        {
            respawnManager.Register(this);
            Save(); // base save
        }

        void OnDestroy()
        {
            respawnManager.Unregister(this);
        }

        /**
         * Called when game reached a checkpoint, used to save data needed for respawn.
         */
        public abstract void Save();

        /**
         * Reset needed properties to the state of last save
         */
        public abstract void Respawn();
    }
}