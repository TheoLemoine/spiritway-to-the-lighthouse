﻿using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Respawn
{
    public class Checkpoint : ATriggerOnce
    {
        protected override void OnTrigger()
        {
            respawnManager.TriggerSave();
        }
    }
}