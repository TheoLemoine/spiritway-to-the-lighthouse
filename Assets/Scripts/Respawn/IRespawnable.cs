﻿namespace Spiritway.Respawn
{
    public interface IRespawnable
    {
        void Save();
        void Respawn();
    }
}