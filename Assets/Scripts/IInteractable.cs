﻿using UnityEngine;

namespace Spiritway
{
    public interface IInteractable
    {
        /**
         * Called by given interactor when trying to start an interaction.
         */
        void StartInteraction(IInteractor interactor, GameObject obj);

        /**
         * Checks if the interactable is available for interaction
         */
        bool CanInteract();

        /**
         * Returns a description of the interaction
         */
        string GetInteractionName();
    }
}