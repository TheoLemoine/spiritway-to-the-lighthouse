﻿using System;
using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Sailor
{
    public class SailorFootsteps : MonoBehaviour
    {
        [SerializeField, TagSelector] string boatTag;
        [SerializeField] AK.Wwise.Event footstepEvent;
        [SerializeField] float footstepDistance;

        // relative to boat transform
        Vector3 _lastPos;
        float _distLastFootstep;
        
        Transform _boatTransform;
        Transform _transform;

        void Start()
        {
            _transform = transform;
            _boatTransform = GameObject.FindWithTag(boatTag).transform;

            _lastPos = _boatTransform.InverseTransformPoint(_transform.position);
        }

        void FixedUpdate()
        {
            var boatRelativePos = _boatTransform.InverseTransformPoint(_transform.position);

            _distLastFootstep += Vector3.Distance(boatRelativePos, _lastPos);
            _lastPos = boatRelativePos;
            
            if (_distLastFootstep > footstepDistance)
            {
                footstepEvent.Post(gameObject);
                _distLastFootstep = 0;
            }
        }
    }
}