﻿using System;
using Crest;
using Spiritway.Boat;
using UnityEngine;

namespace Spiritway.Sailor
{
    public class SailorNotUnderwater : MonoBehaviour
    {
        [SerializeField] BoatHull hull;
        [SerializeField] LayerMask whatIsBoat;
        [SerializeField] float maxRayDistance = 5;

        void FixedUpdate()
        {
            UnderwaterPostProcessHDRP.skipUnderwater =
                Physics.Raycast(transform.position, Vector3.down, maxRayDistance, whatIsBoat);
            
            if (hull.IsDead()) UnderwaterPostProcessHDRP.skipUnderwater = false;
        }
    }
}