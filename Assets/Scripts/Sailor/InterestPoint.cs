﻿using System;
using System.Collections;
using System.Linq;
using Spiritway.Dialogue;
using Spiritway.Player;
using Spiritway.Sound;
using Spiritway.UI;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Spiritway.Sailor
{
    public class InterestPoint : MonoBehaviour
    {
        [SerializeField] UIManager uiManager;
        [SerializeField] SoundManager soundManager;
        [SerializeField, TagSelector] string sailorTag;
        [SerializeField] AK.Wwise.Event interestPointNotifSound;
        [SerializeField] float interactDistance;
        [SerializeField] VisualTreeAsset uiScreen;
        [SerializeField] Transform triggerCenter;
        [SerializeField] bool hasLine;
        [SerializeField] DialogueLine line;

        Transform _transform;
        GameObject _sailor;
        Transform _sailorTransform;
        SailorController _sailorController;

        VisualElement _hudScreen;
        VisualElement _hudTip;
        PlayerActions _actions;

        bool _isInRange;
        Coroutine _glowingCoroutine;

        void Start()
        {
            _transform = transform;
            
            // get sailor
            _sailor = GameObject.FindGameObjectWithTag(sailorTag);
            _sailorTransform = _sailor.transform;
            _sailorController = _sailor.GetComponent<SailorController>();
            
            // setup UI
            _hudScreen = uiScreen.Instantiate().Children().First();
            _hudTip = _hudScreen.Q("tip");
            _glowingCoroutine = StartCoroutine(GlowingOpacitySine(_hudTip, 0f, 1f, 2f));
            
            // setup controlls
            _actions = new PlayerActions();
            _actions.SailorControls.AutoLook.performed += OnLook;
        }

        void OnDestroy()
        {
            Deactivate();
        }

        void OnLook(InputAction.CallbackContext context)
        {
            _sailorController.SmoothLookAt(_transform.position);
            if (hasLine)
            {
                StartCoroutine(TimeHelper.Delay(_sailorController.lookAtDuration, soundManager.PlayLine(line, gameObject)));
            }
            
            Deactivate();
            enabled = false;
        }

        void FixedUpdate()
        {
            var toSailor = triggerCenter.position - _sailorTransform.position;
            var nowInRange = toSailor.sqrMagnitude < interactDistance * interactDistance;
            
            if (nowInRange && !_isInRange)
            {
                _isInRange = true;
                Activate();
            }
            else if (!nowInRange && _isInRange)
            {
                _isInRange = false;
                Deactivate();
            }
        }

        IEnumerator GlowingOpacitySine(VisualElement elem, float minOpacity, float maxOpacity, float timeMultiplier = 1f)
        {
            for (float elapsed = 0; ; elapsed += Time.deltaTime)
            {
                var x = 1 - Mathf.Pow(Mathf.Sin(elapsed * timeMultiplier), 6);
                
                elem.style.unityBackgroundImageTintColor = new Color(1, 1, 1, MathHelpers.Remap(
                    x, 0, 1, minOpacity, maxOpacity
                ));

                yield return null;
            }
        }

        void Activate()
        {
            uiManager.AddScreenToHud(_hudScreen);
            interestPointNotifSound.Post(gameObject);
            _actions.Enable();
        }

        void Deactivate()
        {
            _hudScreen?.RemoveFromHierarchy();
            _actions.Disable();
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(triggerCenter.position, interactDistance);
        }
#endif
    }
}