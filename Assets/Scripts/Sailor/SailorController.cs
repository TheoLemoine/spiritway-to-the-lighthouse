using System.Collections;
using Spiritway.Player;
using Spiritway.Utils;
using Spiritway.Utils.Helpers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spiritway.Sailor
{
    /**
     * Behaviour responsible for moving the sailor character using Player Actions
     */
    [RequireComponent(typeof(Rigidbody))]
    public class SailorController : MonoBehaviour
    {
        public bool IsLocked { get; set; } = false;

        [SerializeField, TagSelector] string boatTag;
        
        [Header("Looking")]
        [SerializeField] Transform cameraTransform;
        [SerializeField] PlayerOptions playerOptions;

        [Header("Moving")]
        [SerializeField] float walkSpeed = 1f;

        [Header("Align to boat")]
        [SerializeField, Range(0f, 1f)] float alignAmount;

        [Header("look at")]
        [SerializeField] public float lookAtDuration;

        PlayerActions _actions;
        Rigidbody _rb;
        Transform _transform;

        Transform _boatTransform;
        
        Vector2 _camRotation = Vector2.zero;
        Vector2 _lookSpeed = Vector2.zero;
        Vector3 _moveSpeed = Vector3.zero;

        public void SmoothLookAt(Vector3 pos)
        {
            // compute needed rotation to look at object
            var toPos = Vector3.Normalize(pos - _transform.position);
            
            var xRotAngle = Vector3.SignedAngle(_boatTransform.forward, toPos, Vector3.up);
            var yRotAngle = Mathf.Asin(toPos.normalized.y) * Mathf.Rad2Deg;

            var newCamRot = new Vector2(xRotAngle, yRotAngle);

            // lerp to the rotation
            StartCoroutine(LerpCamRotationTo(newCamRot, lookAtDuration));
        }

        public IEnumerator LerpCamRotationTo(Vector2 targetCamRotation, float duration)
        {
            var startCamRotation = _camRotation;

            var diffX = MathHelpers.AngleDifference(_camRotation.x, targetCamRotation.x);
            var diffY = MathHelpers.AngleDifference(_camRotation.y, targetCamRotation.y);

            var camRotationDiff = new Vector2(diffX, diffY);
            
            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var progress = Mathf.SmoothStep(0, 1, elapsed / duration);
                _camRotation = startCamRotation + Vector2.Lerp(Vector2.zero, camRotationDiff, progress);
                
                yield return null;
            }
        }

        // -- Lifecycle
        
        void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _transform = GetComponent<Transform>();

            _boatTransform = GameObject.FindWithTag(boatTag).transform;

            _camRotation.x = transform.rotation.eulerAngles.y;
            _camRotation.y = cameraTransform.localRotation.x;

            // hide cursor, and prevent it from leaving window
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        
            // make and bind input events
            _actions = new PlayerActions();

            _actions.SailorControls.Look.performed += OnLook;
            _actions.SailorControls.Look.canceled += OnLook;
            
            _actions.SailorControls.Walk.performed += OnWalk;
            _actions.SailorControls.Walk.canceled += OnWalk;
        }

        void OnEnable()
        {
            _actions.Enable();
        }

        void OnDisable()
        {
            _actions.Disable();
        }

        void FixedUpdate()
        {
            // moving
            if (!IsLocked)
            {
                var nextPos = _transform.TransformPoint(_moveSpeed * (walkSpeed * Time.fixedDeltaTime));
                _rb.MovePosition(nextPos);
            }

            // looking around
            _camRotation += _lookSpeed * Time.fixedDeltaTime;
            
            // -- Clamp rotation up and down
            _camRotation.y = Mathf.Clamp(_camRotation.y, -90f, 90f);
            _camRotation.x %= 360f;
            
            // -- align sailor to boat
            var up = Vector3.Lerp(Vector3.up, _boatTransform.up, alignAmount);
            var alignRotation = Quaternion.Euler(-Mathf.Acos(up.z) * Mathf.Rad2Deg + 90, 0,
                Mathf.Acos(up.x) * Mathf.Rad2Deg - 90);
            
            // -- rotate sideways
            var sidewaysRotation = Quaternion.Euler(0, _camRotation.x + _boatTransform.rotation.eulerAngles.y, 0);

            _rb.MoveRotation(alignRotation * sidewaysRotation);
            
            // -- rotate up and down
            cameraTransform.localRotation = Quaternion.Euler(-_camRotation.y, 0, 0);
        }

        // -- Input callbacks
        
        void OnLook(InputAction.CallbackContext context)
        {
            _lookSpeed = context.ReadValue<Vector2>() * playerOptions.lookSensitivity;
        }

        void OnWalk(InputAction.CallbackContext context)
        {
            var input = context.ReadValue<Vector2>() ;
            
            // make 3D movement from 2D input
            _moveSpeed = new Vector3(input.x, 0, input.y) * walkSpeed;
        }
    }
}

