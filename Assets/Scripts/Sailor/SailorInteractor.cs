﻿using System.Collections;
using System.Linq;
using Spiritway.Player;
using Spiritway.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Spiritway.Sailor
{
    /**
     * Behaviour responsible for detecting and interacting with interactable objects around
     * Works with a raycast
     */
    [RequireComponent(typeof(SailorController))]
    public class SailorInteractor : MonoBehaviour, IInteractor
    {
        public bool IsInteracting { get; private set; } = false;

        [Header("Interaction")]
        [SerializeField] Transform cameraTransform;
        [SerializeField] float interactRange;
        [SerializeField] LayerMask raycastsLayers;

        [Header("UI")] 
        [SerializeField] VisualTreeAsset defaultScreenHud;
        [SerializeField] UIManager uiManager;
        [SerializeField] float uiUpdateFrequency = 10;
        
        SailorController _controller;
        PlayerActions _actions;
        Coroutine _uiUpdateCoroutine;
        VisualElement _hudScreen;

        void Awake()
        {
            _controller = GetComponent<SailorController>();
            
            _hudScreen = defaultScreenHud.Instantiate().Children().First();
            _actions = new PlayerActions();
        }

        void OnEnable()
        {
            _actions.Enable();
            
            _actions.SailorControls.Interact.performed += OnInteract;

            EnableUI();
        }

        void OnDisable()
        {
            _actions.Disable();

            _actions.SailorControls.Interact.performed -= OnInteract;
            
            DisableUI();
        }

        void OnInteract(InputAction.CallbackContext context)
        {
            // no new interaction before the first is finished
            if (IsInteracting) return;
            
            if (TryFindInteractable(out var interactable))
            {
                _controller.IsLocked = true;
                interactable.StartInteraction(this, gameObject);
                IsInteracting = true;
                DisableUI();
            }
            else
            {
                // play a du-dun~ sound or whatever
            }
        }
        
        public void StopCurrentInteraction()
        {
            if (!IsInteracting) Debug.LogWarning("Trying to stop interaction when no interaction is ongoing");

            _controller.IsLocked = false;
            IsInteracting = false;
            EnableUI();
        }

        /**
         * Does a raycast to find the first interactable in sight
         * If none are found, sets interactable to null and returns false
         */
        bool TryFindInteractable(out IInteractable interactable)
        {
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out var hit, interactRange, raycastsLayers) 
                && hit.collider.TryGetComponent(out interactable) 
                && interactable.CanInteract())
            {
                return true;
            }

            // if we reach this point, no interactable were found.
            interactable = null;
            return false;
        }

        void EnableUI()
        {
            uiManager.AddScreenToHud(_hudScreen);
            _uiUpdateCoroutine = StartCoroutine(UpdateUI());
        }

        void DisableUI()
        {
            _hudScreen.RemoveFromHierarchy();
            StopCoroutine(_uiUpdateCoroutine);
        }
        
        IEnumerator UpdateUI()
        {
            for (;;)
            {
                if (TryFindInteractable(out var interactable))
                {
                    _hudScreen.style.visibility = Visibility.Visible;
                    _hudScreen.Q<Label>("interact").text = interactable.GetInteractionName();
                }
                else
                {
                    _hudScreen.style.visibility = Visibility.Hidden;
                }
                
                yield return new WaitForSeconds(1 / uiUpdateFrequency);
            }
        }
    }
}