﻿using System.Collections;
using Spiritway.Respawn;
using Spiritway.UI;
using UnityEngine;

namespace Spiritway.Sailor
{
    [RequireComponent(typeof(BoatAlignNormal))]
    public class SailorDeath : MonoBehaviour
    {
        [SerializeField] UIManager uiManager;
        [SerializeField] RespawnManager respawnManager;
        
        [SerializeField] float timeInWaterBeforeDeath;
        [SerializeField] float fadeToBlackDuration;
        [SerializeField] float blackScreenDuration;

        BoatAlignNormal _floater;
        SailorFootsteps _footsteps;
        Coroutine _deathTimeout;

        void Start()
        {
            _floater = GetComponent<BoatAlignNormal>();
            _footsteps = GetComponent<SailorFootsteps>();
        }

        void Update()
        {
            // deactivate footsteps in water
            _footsteps.enabled = !_floater.InWater;
            
            if (_floater.InWater && _deathTimeout == null)
            {
                _deathTimeout = StartCoroutine(DeathTimeout());
            }
            else if (!_floater.InWater && _deathTimeout != null)
            {
                StopCoroutine(_deathTimeout);
                _deathTimeout = null;
            }
        }

        IEnumerator DeathTimeout()
        {
            yield return new WaitForSeconds(timeInWaterBeforeDeath);

            StartCoroutine(DeathAndRespawn());
        }

        IEnumerator DeathAndRespawn()
        {
            yield return StartCoroutine(uiManager.FadeToBlack(fadeToBlackDuration));
            
            respawnManager.TriggerRespawn();
            yield return new WaitForSeconds(blackScreenDuration);
            
            yield return StartCoroutine(uiManager.FadeFromBlack(fadeToBlackDuration));
        }
    }
}