﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Sailor
{
    public class SailorScreenShake : MonoBehaviour
    {
        [SerializeField] Transform cameraTransform;
        [SerializeField] float shakeSpeed;
        [SerializeField] float shakeForce;
        [SerializeField] float circleSize;
        [SerializeField] float baseOffset;

        public void Shake(float duration)
        {
            StartCoroutine(DoShake(duration));
        }

        IEnumerator DoShake(float duration)
        {
            var appliedOffset = Vector3.zero;
            
            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var offsetX = Mathf.PerlinNoise(
                    baseOffset + Mathf.Cos(elapsed * shakeSpeed) * circleSize, 
                    baseOffset + Mathf.Sin(elapsed * shakeSpeed) * circleSize) 
                    * shakeForce;
                
                var offsetY = Mathf.PerlinNoise(
                    baseOffset + Mathf.Cos(elapsed * shakeSpeed + Mathf.PI) * circleSize, 
                    baseOffset + Mathf.Sin(elapsed * shakeSpeed + Mathf.PI) * circleSize) 
                    * shakeForce;

                var offset = new Vector3(offsetX, offsetY, 0);

                cameraTransform.localPosition -= appliedOffset; // remove previous
                appliedOffset = offset * (1 - elapsed / duration);
                cameraTransform.localPosition += appliedOffset; // add new

                
                yield return null;
            }
        }
    }
}