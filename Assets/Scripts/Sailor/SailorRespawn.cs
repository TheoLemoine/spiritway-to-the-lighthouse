﻿using Spiritway.Respawn;
using UnityEngine;

namespace Spiritway.Sailor
{
    public class SailorRespawn : ARespawnableBehaviour
    {
        Transform _transform;
        Rigidbody _rb;

        Vector3 _savedPosition;
        Quaternion _savedRotation;

        protected override void Start()
        {
            _transform = GetComponent<Transform>();
            _rb = GetComponent<Rigidbody>();
            
            base.Start();
        }

        public override void Save()
        {
            _savedPosition = _transform.position;
            _savedRotation = _transform.rotation;
        }

        public override void Respawn()
        {
            _rb.velocity = Vector3.zero;
            _transform.position = _savedPosition;
            _transform.rotation = _savedRotation;
        }
    }
}