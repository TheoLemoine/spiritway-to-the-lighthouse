﻿using Spiritway.UI;
using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Dialogue
{
    public class PlayDialogueLine : ATriggerOnce
    {
        [SerializeField] UIManager uiManager;
        [SerializeField] DialogueLine line;
        
        protected override void OnTrigger()
        {
            line.voiceEvent.Post(gameObject);
            StartCoroutine(uiManager.SetSubtitle(line.subtitle, line.duration));
        }
    }
}