﻿using UnityEngine;

namespace Spiritway.Dialogue
{
    [CreateAssetMenu(fileName = "_line_", menuName = "Spiritway/Dialogue Line", order = 0)]
    public class DialogueLine : ScriptableObject
    {
        public AK.Wwise.Event voiceEvent;
        public string subtitle;
        public float duration;
    }
}