﻿using System.Collections;
using System.Collections.Generic;
using AK.Wwise;
using Spiritway.Dialogue;
using Spiritway.Respawn;
using Spiritway.UI;
using UnityEngine;

namespace Spiritway.Sound
{
    [CreateAssetMenu(fileName = "SoundManager", menuName = "Spiritway/Managers/Sound Manager", order = 0)]
    public class SoundManager : ScriptableObject, IRespawnable
    {
        [SerializeField] public UIManager uiManager;
        public enum StateTypes
        {
            Wind,
            Rain,
            Ocean,
            Storm,
            Seagulls,
            Music,
        }
        
        Dictionary<StateTypes, State> CurrentStates { get; set; }
        Dictionary<StateTypes, State> SavedStates { get; set; }

        void OnEnable()
        {
            CurrentStates = new Dictionary<StateTypes, State>();
            SavedStates = new Dictionary<StateTypes, State>();
        }

        public IEnumerator PlayLine(DialogueLine line, GameObject gameObject)
        {
            line.voiceEvent.Post(gameObject);
            yield return uiManager.SetSubtitle(line.subtitle, line.duration);
        }

        public void SetCurrentState(StateTypes type, State state)
        {
            CurrentStates[type] = state;
            state.SetValue();
        }

        public void Save()
        {
            // simple copy
            foreach (var state in CurrentStates)
            {
                SavedStates[state.Key] = state.Value;
            }
        }

        public void Respawn()
        {
            foreach (var state in SavedStates)
            {
                state.Value.SetValue();
            }
        }
    }
}