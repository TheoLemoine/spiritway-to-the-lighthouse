﻿using System;
using UnityEngine;

namespace Spiritway.Sound
{
    public class WwiseStateOnStart : MonoBehaviour
    {
        [SerializeField] SoundManager manager;
        [SerializeField] SoundManager.StateTypes type;
        [SerializeField] AK.Wwise.State state;

        void Start()
        {
            manager.SetCurrentState(type, state);
        }
    }
}