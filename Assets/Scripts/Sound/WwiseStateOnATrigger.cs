﻿using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Sound
{
    public class WwiseStateOnATrigger : ATriggerOnce
    {
        [SerializeField] SoundManager manager;
        [SerializeField] SoundManager.StateTypes type;
        [SerializeField] AK.Wwise.State state;
        
        protected override void OnTrigger()
        {
            manager.SetCurrentState(type, state);
        }
    }
}