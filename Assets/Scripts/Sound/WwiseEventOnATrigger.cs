﻿using Spiritway.Utils;
using UnityEngine;

namespace Spiritway.Sound
{
    public class WwiseEventOnATrigger : ATriggerOnce
    {
        [SerializeField] AK.Wwise.Event wwiseEvent;
        
        protected override void OnTrigger()
        {
            wwiseEvent.Post(gameObject);
        }
    }
}