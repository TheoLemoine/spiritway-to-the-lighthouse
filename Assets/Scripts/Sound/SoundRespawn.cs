﻿using Spiritway.Respawn;
using Spiritway.Sound;
using UnityEngine;

namespace Spiritway.Utils
{
    public class SoundRespawn : ARespawnableBehaviour
    {
        [SerializeField] SoundManager soundManager;

        public override void Save()
        {
            soundManager.Save();
        }

        public override void Respawn()
        {
            soundManager.Respawn();
        }
    }
}