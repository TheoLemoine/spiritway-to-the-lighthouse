﻿using Spiritway.Respawn;
using UnityEngine;

namespace Spiritway.Utils
{
    /**
     * Base class for triggers who are used once and then disappear.
     */
    [RequireComponent(typeof(Collider))]
    public abstract class ATriggerOnce : ARespawnableBehaviour
    {
        Collider _collider;
        protected bool Activated = true;
        protected bool SavedActivated;
        
        protected override void Start()
        {
            _collider = GetComponent<Collider>();
            
            base.Start();
            
#if UNITY_EDITOR
            if (!_collider.isTrigger)
            {
                Debug.LogWarning($"{gameObject.name}: Attached collider should be set to isTrigger in order to work");
            }
#endif
        }
        
        public override void Save()
        {
            SavedActivated = Activated;
        }

        public override void Respawn()
        {
            Activated = SavedActivated;
        }

        void OnTriggerEnter(Collider other)
        {
            if (Activated)
            {
                OnTrigger();
                Activated = false;
            }
        }

        protected abstract void OnTrigger();

    }
}