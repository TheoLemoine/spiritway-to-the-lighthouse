﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Utils.Helpers
{
    public static class TimeHelper
    {
        /**
         * Routine that delay a function call
         */
        public static IEnumerator Delay(float seconds, Action callback)
        {
            yield return new WaitForSeconds(seconds);
            callback();
        }

        /**
         * Routine that delay another routine
         */
        public static IEnumerator Delay(float seconds, IEnumerator routine)
        {
            yield return new WaitForSeconds(seconds);
            yield return routine;
        }
    }
}