﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

namespace Spiritway.Utils.Helpers
{
    public static class UIHelper
    {
        public static IEnumerator SmoothLerpOpacity(VisualElement elem, float fromOpacity, float targetOpacity, float duration)
        {
            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / duration;
                elem.style.opacity = Mathf.SmoothStep(fromOpacity, targetOpacity, progress);
                
                yield return null;
            }
            
            elem.style.opacity = targetOpacity;
        }
    }
}