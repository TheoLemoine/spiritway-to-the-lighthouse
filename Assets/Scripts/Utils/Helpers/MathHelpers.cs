﻿namespace Spiritway.Utils.Helpers
{
    public static class MathHelpers
    {
        public static float AngleDifference( float from, float to )
        {
            var diff = ( to - from + 180 ) % 360 - 180;
            return diff < -180 ? diff + 360 : diff;
        }
        
        public static float Remap (float value, float from1, float to1, float from2, float to2) {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}