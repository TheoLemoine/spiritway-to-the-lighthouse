﻿using System.Collections;
using UnityEngine;

namespace Spiritway.Utils.Helpers
{
    public static class LightHelper
    {
        public static IEnumerator LerpLightIntensity(Light light, float targetIntensity, float duration)
        {
            var startIntensity = light.intensity;

            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / duration;

                light.intensity = Mathf.Lerp(startIntensity, targetIntensity, progress);

                yield return null;
            }

            light.intensity = targetIntensity;
        }
    }
}