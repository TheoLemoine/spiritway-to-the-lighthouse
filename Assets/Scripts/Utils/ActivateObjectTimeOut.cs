﻿using System;
using Spiritway.Utils.Helpers;
using UnityEngine;

namespace Spiritway.Utils
{
    public class ActivateObjectTimeOut : MonoBehaviour
    {
        [SerializeField] MonoBehaviour behaviourToActivate;
        [SerializeField] float delayDuration;

        void Start()
        {
            if (behaviourToActivate.enabled)
            {
                Debug.LogWarning($"Behaviour on {behaviourToActivate.gameObject.name} was already activated, deactivating...");
                behaviourToActivate.enabled = false;
            }
            
            StartCoroutine(TimeHelper.Delay(delayDuration, () =>
            {
                behaviourToActivate.enabled = true;
            }));
        }
    }
}