﻿using System;
using UnityEngine;

namespace Spiritway.Utils
{
    [CreateAssetMenu(fileName = "Event", menuName = "Spiritway/Global Event", order = 0)]
    public class GlobalEvent : ScriptableObject
    {
        public event Action OnCalled;
        public bool WasTriggered { get; set; } = false;

        void OnEnable()
        {
            WasTriggered = false;
        }

        public void Call()
        {
            WasTriggered = true;
            OnCalled?.Invoke();
        }
    }
}