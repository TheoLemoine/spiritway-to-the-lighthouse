﻿using UnityEngine;

namespace Spiritway.Utils
{
    public class TagSelectorAttribute : PropertyAttribute
    {
        public bool UseDefaultTagFieldDrawer = false;
    }
}
