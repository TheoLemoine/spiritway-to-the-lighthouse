﻿using System.Collections;
using UnityEngine;

namespace Spiritway.Utils
{
    public class SetLightIntensity : ATriggerOnce
    {
        [SerializeField, TagSelector] string lightTag;
        [SerializeField] float transitionDuration;
        [SerializeField] float targetIntensity;

        Light _light;
        float _baseIntensity;

        protected override void Start()
        {
            base.Start();

            _light = GameObject.FindWithTag(lightTag).GetComponentInChildren<Light>();
            _baseIntensity = _light.intensity;
        }
        
        public override void Respawn()
        {
            base.Respawn();
            
            if (SavedActivated)
            {
                _light.intensity = _baseIntensity;
            }
        }

        protected override void OnTrigger()
        {
            StartCoroutine(TurnOff());
        }

        IEnumerator TurnOff()
        {
            for (float elapsed = 0; elapsed < transitionDuration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / transitionDuration;
                _light.intensity = Mathf.Lerp(_baseIntensity, targetIntensity, progress);
                yield return null;
            }

            _light.intensity = targetIntensity;
        }

    }
}