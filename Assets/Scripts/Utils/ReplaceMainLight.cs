﻿using System;
using System.Collections;
using UnityEngine;

namespace Spiritway.Utils
{
    public class ReplaceMainLight : MonoBehaviour
    {
        [SerializeField, TagSelector] string mainLightTag;
        [SerializeField] Light newMainLight;
        void Start()
        {
            var oldMainLight = GameObject.FindWithTag(mainLightTag).GetComponent<Light>();
            oldMainLight.shadows = LightShadows.None;
            oldMainLight.tag = "Untagged";
            
            StartCoroutine(LerpIntensity(oldMainLight, oldMainLight.intensity, 0, 5, () => oldMainLight.enabled = false));
            StartCoroutine(LerpIntensity(newMainLight, 0, newMainLight.intensity, 5, () => newMainLight.gameObject.tag = mainLightTag));
        }

        IEnumerator LerpIntensity(Light light, float from, float to, float duration, Action onEnded)
        {
            for (float elapsed = 0; elapsed < duration; elapsed += Time.deltaTime)
            {
                var progress = elapsed / duration;
                light.intensity = Mathf.Lerp(from, to, progress);
                yield return null;
            }

            onEnded();
        }
    }
}