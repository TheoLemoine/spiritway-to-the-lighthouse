﻿using System;
using UnityEngine;

namespace Spiritway.Utils
{
    public class ResetGlobalEventOnStart : MonoBehaviour
    {
        [SerializeField] GlobalEvent globalEvent;

        private void Start()
        {
            globalEvent.WasTriggered = false;
        }
    }
}