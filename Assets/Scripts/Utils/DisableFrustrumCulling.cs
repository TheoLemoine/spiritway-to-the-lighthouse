﻿using UnityEngine;

namespace Spiritway.Utils
{
    public class DisableFrustrumCulling : MonoBehaviour
    {
        Camera _cam;
 
        void Start()
        {
            _cam = GetComponent<Camera>();
        }
 
        void OnPreCull()
        {
            _cam.cullingMatrix = Matrix4x4.Ortho(-99999, 99999, -99999, 99999, 0.001f, 99999) * 
                                Matrix4x4.Translate(Vector3.forward * -99999 / 2f) * 
                                _cam.worldToCameraMatrix;
        }
 
        void OnDisable()
        {
            _cam.ResetCullingMatrix();
        }
    }
}