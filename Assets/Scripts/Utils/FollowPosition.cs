﻿using UnityEngine;

namespace Spiritway.Utils
{
    public class FollowPosition : MonoBehaviour
    {
        [SerializeField] Transform followTarget;
        [SerializeField] bool followHeight = true;
        
        Transform _transform;
        Vector3 _offset;
        Quaternion _rotationOffset;

        void Start()
        {
            _transform = GetComponent<Transform>();
            _offset = followTarget.position - _transform.position;
        }

        void FixedUpdate()
        {
            var absPos = followTarget.position - _offset;

            _transform.position = followHeight ? absPos : new Vector3(absPos.x, _transform.position.y, absPos.z);
        }
    }
}