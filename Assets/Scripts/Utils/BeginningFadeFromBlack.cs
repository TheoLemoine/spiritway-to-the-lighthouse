﻿using System;
using System.Collections;
using Spiritway.Dialogue;
using Spiritway.UI;
using UnityEngine;

namespace Spiritway.Utils
{
    public class BeginningFadeFromBlack : MonoBehaviour
    {
        [SerializeField] UIManager manager;
        [SerializeField] DialogueLine line;
        [SerializeField] float beforeLineDuration = 1f;
        [SerializeField] float duration = 3f;


        void Awake()
        {
            manager.SetBlackPanelOpacity(1f);
        }

        IEnumerator Start()
        {
            yield return new WaitForSeconds(beforeLineDuration);
            line.voiceEvent.Post(gameObject);
            yield return manager.SetSubtitle(line.subtitle, line.duration);
            yield return manager.FadeFromBlack(duration);
        }
    }
}