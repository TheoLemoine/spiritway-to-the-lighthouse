﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Spiritway.Utils
{
    public class ButtonMaterialAnimation : MonoBehaviour
    {
        [SerializeField] Image buttonImage;
        [SerializeField] TMP_Text text;

        [SerializeField, ColorUsage(false, true)] Color textColor;
        [SerializeField, Range(0f, 1f)] float smokeVisibility;

        void Awake()
        {
            buttonImage.material = new Material(buttonImage.material);
        }

        void Update()
        {
            buttonImage.material.SetFloat("Visibility", smokeVisibility);

            text.color = textColor;
        }
    }
}