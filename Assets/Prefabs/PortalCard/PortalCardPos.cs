using Spiritway.Utils;
using UnityEngine;

namespace Spiritway
{
    public class PortalCardPos : MonoBehaviour
    {
        private static readonly int SelfPos = Shader.PropertyToID("SelfPos");
        private static readonly int BoatPos = Shader.PropertyToID("BoatPos");
        
        [SerializeField, TagSelector] string boatTag;
        [SerializeField] Material portalCardMat;
        
        Transform _boatTransform;

        // TODO: optimise, distance is calculated in each fragment, were it could be computed once in CPU and then send to shader.
        
        void Start()
        {
            // find boat
            _boatTransform = GameObject.FindWithTag(boatTag).transform;
            
            // send our own world position to the shader
            portalCardMat.SetVector(SelfPos, transform.position);
        }
        
        void Update()
        {
            // send boat position to the shader
            portalCardMat.SetVector(BoatPos, _boatTransform.position);
        }
    }
}
