using Spiritway.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spiritway
{
    public class GhostField : MonoBehaviour
    {
        [SerializeField, TagSelector] string boatTag;

        Transform _boatTransform;

        Material _mat;

        // Start is called before the first frame update
        void Start()
        {
            _mat = transform.GetComponent<Renderer>().material;
            _boatTransform = GameObject.FindWithTag(boatTag).transform;
        }

        // Update is called once per frame
        void Update()
        {
            float _dist = Vector3.Distance(_boatTransform.position, transform.position);
            Debug.Log(_dist);
            _mat.SetFloat("Distance", _dist);
        }
    }
}
