Shader "CustomRenderTexture/Raindrop"
{
    Properties
    { }

    SubShader
    {
        Lighting Off
        Blend One Zero

        Pass
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0

            // x and y are rd pos, z is its size.
            float4 raindrops[32];

            static float2 neighbours[8] = {
                float2(1, 0),
                float2(0, 1),
                float2(-1, 0),
                float2(0, -1),
                
                float2(1, 1),
                float2(1, -1),
                float2(-1, 1),
                float2(-1, -1),
            };

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
                // generate raindrops at current positions
                float ratio = _CustomRenderTextureInfo.x / _CustomRenderTextureInfo.y;
                float4 self = tex2D(_SelfTexture2D, IN.globalTexcoord.xy);
                float height = self.a;
                for (int i = 0; i < 32; i++)
                {
                    float3 raindrop = raindrops[i];
                    if(raindrop.z <= 0 || raindrop.y <= 0) continue;
                    
                    float diffX = (raindrop.x - IN.globalTexcoord.x) / raindrop.z * ratio;
                    float diffY = (raindrop.y - IN.globalTexcoord.y) / raindrop.z;

                    float sqrDist = diffX * diffX + diffY * diffY;

                    if(sqrDist < 1) {
                        height = max(height, sqrt(1 - sqrDist));
                    }
                }

                // recompute normals from previous texture
                float2 stepX = float2(1, 0) / _CustomRenderTextureInfo.xy;
                float2 stepY = float2(0, 1) / _CustomRenderTextureInfo.xy;
                float nextXHeight = tex2D(_SelfTexture2D, IN.globalTexcoord.xy + stepX).a;
                float nextYHeight = tex2D(_SelfTexture2D, IN.globalTexcoord.xy + stepY).a;
                float3 toNextXVec = float3(self.xy + stepX, nextXHeight) - self.rga;
                float3 toNextYVec = float3(self.xy + stepY, nextYHeight) - self.rga;
                float3 normal = normalize(cross(toNextXVec, toNextYVec));

                
                // disappearing outside of raindrops
                for (int j = 0; j < 8; j++)
                {
                    float2 uv_neighbour = IN.globalTexcoord.xy + neighbours[j] / _CustomRenderTextureInfo.xy;
                    float4 neighbour = tex2D(_SelfTexture2D, uv_neighbour);

                    if(neighbour.a < self.a)
                    {
                        height += 0.03 * (neighbour.a - self.a);
                    }
                }

                if(height < 0.01)
                {
                    height = 0;
                }
                
                return float4(normal, height);
            }
            ENDCG
        }
    }
}