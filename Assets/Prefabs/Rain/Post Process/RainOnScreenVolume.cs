using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace Spiritway
{
    [Serializable, VolumeComponentMenu("Post-processing/Custom/RainPPVolume")]
    public sealed class RainOnScreenVolume : CustomPostProcessVolumeComponent, IPostProcessComponent
    {
        const string ShaderName = "Hidden/Shader/RainOnScreen";
        Material _material;
        
        public RenderTextureParameter rainTexture = new RenderTextureParameter(null);
        public ClampedFloatParameter refraction = new ClampedFloatParameter(5f, 0f, 10f);
        
        public bool IsActive() => _material != null && rainTexture != null;

        // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > HDRP Default Settings).
        public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

        public override void Setup()
        {
            if (Shader.Find(ShaderName) != null)
                _material = new Material(Shader.Find(ShaderName));
            else
                Debug.LogError($"Unable to find shader '{ShaderName}'. Post Process Volume RainPostProcessVolume is unable to load.");
        }

        public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
        {
            if (_material == null)
                return;

            _material.SetTexture("_InputTexture", source);
            _material.SetTexture("_RainTexture", rainTexture.value);
            _material.SetFloat("_RefractionOffset", refraction.value);
            HDUtils.DrawFullScreen(cmd, _material, destination);
        }

        public override void Cleanup()
        {
            CoreUtils.Destroy(_material);
        }
    }
}
