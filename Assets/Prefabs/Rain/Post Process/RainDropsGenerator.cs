using System;
using Spiritway.Weather;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Spiritway
{
    [ExecuteAlways]
    public class RainDropsGenerator : MonoBehaviour
    {
        [SerializeField] CustomRenderTexture renderTexture;
        [SerializeField] Vector3 gravity = new Vector3(0.05f, -0.2f, -0.05f);
        [SerializeField] float raindropsPerSeconds = 2f;
        [SerializeField] float minRaindropSize = 0.01f;
        [SerializeField] float maxRaindropSize = 0.03f;
        [SerializeField] Vector3 continuousRandomAmount = Vector3.zero;
        [SerializeField] Vector3 stepRandomAmount = Vector3.zero;
        [SerializeField] float minTimeBetweenForceChange = 0.3f;
        [SerializeField] float maxTimeBetweenForceChange = 1.5f;

        int _raindropsShaderID;
        float _textureRatio;
        Vector4[] _raindrops = new Vector4[32];
        Vector4[] _raindropsRandomForce = new Vector4[32];
        float[] _updateTimeRaindrop = new float[32];
        bool[] _activeRaindrops = new bool[32];

        public float RainDropsPerSeconds
        {
            get => raindropsPerSeconds;
            set => raindropsPerSeconds = value;
        }

        public bool IsGenerating { get; set; } = true;

        void Start()
        {
            _raindropsShaderID = Shader.PropertyToID("raindrops");
            _textureRatio = renderTexture.width / (float)renderTexture.height;
            renderTexture.Initialize();
        }

        void Update()
        {
            UpdateRaindrops();

            if (IsGenerating && Random.Range(0f, 1f) < Time.deltaTime * raindropsPerSeconds)
            {
                AddRaindrop();
            }
            
            renderTexture.material.SetVectorArray(_raindropsShaderID, _raindrops);
            renderTexture.Update();
        }

        void UpdateRaindrops()
        {
            var fixedGravity = new Vector4(gravity.x * _textureRatio, gravity.y, gravity.z, 0);
            
            for (int i = 0; i < _activeRaindrops.Length; i++)
            {
                if (_activeRaindrops[i])
                {
                    _updateTimeRaindrop[i] -= Time.deltaTime;
                    if (_updateTimeRaindrop[i] < 0)
                    {
                        _raindropsRandomForce[i] = new Vector4(
                            Random.Range(-stepRandomAmount.x, stepRandomAmount.x),
                            Random.Range(-stepRandomAmount.y, stepRandomAmount.y),
                            Random.Range(-stepRandomAmount.z, 0),
                            0);

                        _updateTimeRaindrop[i] = Random.Range(minTimeBetweenForceChange, maxTimeBetweenForceChange);
                    }
                        
                    _raindrops[i] += fixedGravity * Time.deltaTime;
                    _raindrops[i] += new Vector4(
                        Random.Range(-continuousRandomAmount.x, continuousRandomAmount.x), 
                        Random.Range(-continuousRandomAmount.y, continuousRandomAmount.y), 
                        Random.Range(-continuousRandomAmount.z, continuousRandomAmount.z), 
                        0) * Time.deltaTime;
                    
                    _raindrops[i] += _raindropsRandomForce[i] * Time.deltaTime;
                    
                    // kill raindrop
                    if (_raindrops[i].y < 0 || _raindrops[i].w < 0)
                    {
                        _activeRaindrops[i] = false;
                        _raindrops[i] = Vector4.zero;
                    }
                }
            }
        }

        void AddRaindrop()
        {
            // find first inactive raindrop
            for (int i = 0; i < _activeRaindrops.Length; i++)
            {
                if (!_activeRaindrops[i])
                {
                    _raindrops[i] = new Vector4(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(minRaindropSize, maxRaindropSize), 0);
                    _activeRaindrops[i] = true;
                    
                    break;
                }
            }
        }
    }
}
